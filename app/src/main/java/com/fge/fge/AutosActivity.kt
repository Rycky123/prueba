package com.fge.fge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.activity_autos.*

class AutosActivity : AppCompatActivity() {

    private val BASE_URL="https://fiscaliamichoacan.gob.mx/VehiculosRecuperados/"
    internal lateinit var viewDialog: ViewDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_autos)

        val toolbar = findViewById<Toolbar>(R.id.toolbarVehiculos)
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)


        viewDialog = ViewDialog(this)

        val settings=webView.settings
        settings.javaScriptEnabled=true
        webView!!.webViewClient = Mybrowser(viewDialog)
        webView.loadUrl(BASE_URL)
    }


    override fun onBackPressed() {
        if(webView.canGoBack()){
            webView.goBack()
        }else{
            super.onBackPressed()
            finish()
            overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
        }

    }
    override fun onSupportNavigateUp(): Boolean {
        super.onBackPressed()
        finish()
        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)


        return true
    }
    inner class Mybrowser(internal var pd: ViewDialog) : WebViewClient() {
        init {
            pd.showDialog()
        }

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return super.shouldOverrideUrlLoading(view, url)
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            //if (pd.is) {
                pd.hideDialog()
            //}
        }

        override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
            super.onReceivedError(view, errorCode, description, failingUrl)
            //Toast.makeText(applicationContext, "Error:$description", Toast.LENGTH_SHORT).show()
        }
    }


}
