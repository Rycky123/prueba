package com.fge.fge

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.fge.fge.Adapters.ViewPagerAdapter
import com.fge.fge.Models.GlobalClass
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import io.github.yavski.fabspeeddial.FabSpeedDial
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter
import kotlinx.android.synthetic.main.activity_detalles.*
import kotlinx.android.synthetic.main.activity_predenuncia_resumen.*
import kotlinx.android.synthetic.main.activity_detalles.tpo_denuncia as tpo_denuncia1
import com.google.firebase.database.DataSnapshot
import kotlinx.android.synthetic.main.boton_llamada.*
import com.firebase.ui.auth.data.model.User
import com.squareup.picasso.Picasso
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener
import kotlinx.android.synthetic.main.cancelar_predenuncia.view.*


class PredenunciaResumen : AppCompatActivity() {
    private lateinit var userBD: DatabaseReference
    private lateinit var userBDCANCELADA: DatabaseReference
    private lateinit var database: FirebaseDatabase//para usar la bd
    private lateinit var dbReference: DatabaseReference//firebase
    lateinit var textFecha:TextView
    lateinit var textUbicacion:TextView
    lateinit var textNarrativa:TextView
    lateinit var AUDIObUTTON:Button
    lateinit var AUDIObUTTONP:Button
    val resultado = ArrayList<String>()
    lateinit var tituloEvidencia:TextView
    internal var carouselView2: CarouselView?=null

    lateinit var botonVolverLista:Button
    lateinit var botoncancelarDenuncia:Button
    lateinit var estatus:TextView
    lateinit var linearLayout:LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_predenuncia_resumen)
        scrollView3.setVerticalScrollBarEnabled(false)
        scrollView3.setHorizontalScrollBarEnabled(false)

        val valor = intent.extras!!.getString("dato")
        //Toast.makeText(this, ""+valor, Toast.LENGTH_SHORT).show()

        tpo_denuncia.text=valor

        textFecha=findViewById(R.id.fecha)
        textUbicacion=findViewById(R.id.ubicacionResumen)
        textNarrativa=findViewById(R.id.narrativaEscritaResumen)
        AUDIObUTTON=findViewById(R.id.audioButton)
        AUDIObUTTONP=findViewById(R.id.audioButtonParar)
        tituloEvidencia=findViewById(R.id.tituloEvidencia)
        botonVolverLista=findViewById(R.id.volverLista)
        botoncancelarDenuncia=findViewById(R.id.cancelarDenuncia)
        estatus=findViewById(R.id.estatusResumen)
        linearLayout=findViewById(R.id.bottom_panel)

        var mediaPlayer: MediaPlayer?=null

        val uid = FirebaseAuth.getInstance().currentUser!!.uid
        database = FirebaseDatabase.getInstance()
        dbReference = database.reference.child("Usuarios/")
        userBD = dbReference.child(uid).child("denuncias")

        val query2 = userBD.orderByChild("Id").equalTo(valor)
        query2.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                println("DENUNCIA: ${snapshot.value}")
                if (snapshot.exists()){
                    val children = snapshot!!.children

                    children.forEach {

                        val Fecha=it.child("Fecha").value.toString()
                        textFecha.text=Fecha
                        val tipoDenuncia=it.child("TipoDenuncia").value.toString()
                        tipo_Denuncia.text=tipoDenuncia

                        val Ubicacion=it.child("Ubicacion").value.toString()
                        textUbicacion.text=Ubicacion

                        val Narrativa=it.child("Narrativa").value.toString()
                        textNarrativa.setText(Narrativa)

                        if(it.child("NarrativaVoz").exists()){
                            idAudioVista.visibility=View.VISIBLE
                            val Audio=it.child("NarrativaVoz").value.toString()
                            AUDIObUTTON.setEnabled(true)
                            AUDIObUTTON.setOnClickListener({
                                AUDIObUTTONP.isEnabled=true

                                try{

                                    mediaPlayer = MediaPlayer.create(baseContext, Uri.parse(Audio))
                                    mediaPlayer!!.start()

                                }catch (e: ExceptionInInitializerError){
                                    e.printStackTrace()
                                }
                            })

                            AUDIObUTTONP.setOnClickListener({
                                if (mediaPlayer != null) {
                                    mediaPlayer!!.stop();
                                }
                                AUDIObUTTONP.isEnabled=false
                            })
                        }else{

                        }


                        if(it.child("Evidencia").exists()){

                            //val Fotos=it.child("Evidencia").children



                            for ( hola in it.child("Evidencia").children) {
                                val value = hola.getValue(String::class.java)
                                resultado.add(value.toString())
                            }
                           // Toast.makeText(baseContext, "RESULTADO"+resultado, Toast.LENGTH_SHORT).show()

                            val viewPager = findViewById<ViewPager>(R.id.view_Pager)
                            tituloEvidencia.text= "Evidencia fotográfica"+"("+resultado.size.toString()+")"
                            viewPager!!.visibility=View.VISIBLE
                            tituloEvidencia.visibility=View.VISIBLE
                            val adapter = ViewPagerAdapter(baseContext, resultado!!)
                            viewPager.adapter = adapter

                            val dotsCount =viewPager.childCount
                            //Toast.makeText(baseContext, "Numero de: "+dotsCount, Toast.LENGTH_LONG).show()



                        }else{


                        }

                        if (it.child("Video").exists()){
                            videoTituloPredenuncia.visibility=View.VISIBLE
                            val video=it.child("Video").value.toString()
                            videoFinal2.visibility=View.VISIBLE
                            videoVistaBotones2.visibility=View.VISIBLE
                            val uri= Uri.parse(video)
                            videoFinal2.setVideoURI(uri)
                            videoFinal2.start()
                            videoReproducir2.setOnClickListener({
                                val uri= Uri.parse(video)
                                videoFinal2.setVideoURI(uri)
                                videoFinal2.start()
                            })

                            videoFinal2.setOnCompletionListener {
                                videoFinal2.start()
                            }

                            videoParar2.setOnClickListener({
                                if(videoFinal2.isPlaying){
                                    videoFinal2.stopPlayback()
                                }
                            })

                        }else{
                            //Toast.makeText(baseContext,"No existe VIDEO", Toast.LENGTH_SHORT).show()
                        }





                    }
                }else{
                    //Toast.makeText(baseContext,"No existe ", Toast.LENGTH_SHORT).show()
                }

            }


            override fun onCancelled(error: DatabaseError) {}
        })

        botonVolverLista.setOnClickListener({
            if(videoFinal2.isPlaying){
                videoFinal2.stopPlayback()
            }
            finish()
            overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
        })


        userBDCANCELADA=dbReference.child(uid).child("denuncias").child(valor.toString()).child("Estatus")
        userBDCANCELADA.addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
               if (p0.exists()){
                   linearLayout.visibility=View.GONE
                   estatus.text="CANCELADA"

               }else{
                   linearLayout.visibility=View.VISIBLE
               }
            }

        }

        )

        botoncancelarDenuncia.setOnClickListener({
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.cancelar_predenuncia, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val  mAlertDialog = mBuilder.show()
            mDialogView.seguirAlert.setOnClickListener {

                mAlertDialog.dismiss()
            }
            mDialogView.siAlert.setOnClickListener {
                dbReference.child(uid).child("denuncias").child(valor.toString()).child("Estatus").setValue("CANCELADA")
                mAlertDialog.dismiss()

            }

        })


    }
}
