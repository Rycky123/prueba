package com.fge.fge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.activity_terminos_condiciones.*

class TerminosCondicionesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terminos_condiciones)

        val toolbar = findViewById<Toolbar>(R.id.toolTerminos)
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
    }

    override fun onSupportNavigateUp(): Boolean {
        super.onBackPressed()
        finish()
        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
        return true
    }
}
