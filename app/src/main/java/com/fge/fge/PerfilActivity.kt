package com.fge.fge

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.Path
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.net.toUri
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.activity_autos.*
import kotlinx.android.synthetic.main.activity_perfil.*
import kotlinx.android.synthetic.main.activity_session.*
import java.io.File
import java.lang.Exception
import java.util.*

@Suppress("UNREACHABLE_CODE")
class PerfilActivity : AppCompatActivity() {

   /* override fun onStart() {
        super.onStart()
        retrieveFoto()
    }*/



    private lateinit var txtNombreUserPerf: EditText
    private lateinit var txtApellidoUserPerf: EditText
    private lateinit var txtApellidoMUserPerf: EditText
    private lateinit var txtMunicipioUserPerf: EditText
    private lateinit var txtCURPUser: EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var imagen: ImageView
    private lateinit var userBD: DatabaseReference
    private lateinit var database: FirebaseDatabase
    var firebaseUser: FirebaseUser?=null
    var nombre:String=""
    var apellido:String=""
    var apellidoM:String=""
    var ciudad:String=""
    var curp:String=""
    private lateinit var dbReference: DatabaseReference//firebase
    lateinit var Guardar: Button
    private var imageRef: StorageReference? = null
    var popUpMenu:PopupMenu?=null
    var imageUri: Uri?=null

    var boleanoImagen:Boolean=false
    var link:String?=null
    internal lateinit var viewDialog: ViewDialog

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil)


        txtNombreUserPerf = findViewById(R.id.txtNombreUser)
        txtApellidoUserPerf = findViewById(R.id.txtApellidoUser)
        txtApellidoMUserPerf = findViewById(R.id.txtApellidoMUser)
        txtMunicipioUserPerf = findViewById(R.id.txtCiudadUser)
        txtCURPUser = findViewById(R.id.txtCURPUser)
        imagen=findViewById(R.id.imagenPerfil)


        txtNombreUserPerf = findViewById(R.id.txtNombreUser)
        txtApellidoUserPerf = findViewById(R.id.txtApellidoUser)
        txtApellidoMUserPerf = findViewById(R.id.txtApellidoMUser)
        txtMunicipioUserPerf = findViewById(R.id.txtCiudadUser)
        txtCURPUser = findViewById(R.id.txtCURPUser)
        imagen=findViewById(R.id.imagenPerfil)


        progressBar = ProgressBar(this)
        firebaseUser=FirebaseAuth.getInstance().currentUser

        progressBar = findViewById(R.id.progressBarUser)
        Guardar=findViewById(R.id.cerrarSesionPerfil)

        val uid = FirebaseAuth.getInstance().currentUser!!.uid
        database = FirebaseDatabase.getInstance()
        dbReference = database.reference.child("Usuarios/")
        userBD = dbReference.child(uid)

        val toolbar = findViewById<Toolbar>(R.id.toolbarPerfil)
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)


        viewDialog = ViewDialog(this)

        val numeroFirebase: String? = FirebaseAuth.getInstance().currentUser!!.phoneNumber
        //Toast.makeText(this, ""+numeroFirebase, Toast.LENGTH_LONG).show()
        txtNumeroUser.setText(numeroFirebase)

        retrieveFoto()

        imagen.setOnClickListener({
             popUpMenu = PopupMenu(this, it)

            popUpMenu!!.setOnMenuItemClickListener { item->
                when(item.itemId){
                    R.id.cameraOption->{
                        val values =  ContentValues()
                        values.put(MediaStore.Images.Media.TITLE, "New Picture")
                        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera")

                        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                        intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                        startActivityForResult(intent, 5892)

                        true



                    }
                    R.id.galleryOption->{
                            try {
                                val intent = Intent()
                                intent.type = "image/*"
                                intent.action = Intent.ACTION_GET_CONTENT
                                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 502)
                            } catch (e: Exception) {
                                val photoPickerIntent = Intent(this, PerfilActivity::class.java)
                                startActivityForResult(photoPickerIntent, 502)
                            }


                        true
                    }
                    R.id.borrarImagenMenu->{
                        borrarImagen()
                        true
                    }
                    else->false
                }

            }
            popUpMenu!!.inflate(R.menu.menu_camera)
            if (boleanoImagen==true){
                popUpMenu!!.menu.findItem(R.id.borrarImagenMenu).isVisible=true
            }else{
                popUpMenu!!.menu.findItem(R.id.borrarImagenMenu).isVisible=false
            }

            try {
                val fieldMPopup= PopupMenu::class.java.getDeclaredField("mPopup")
                fieldMPopup.isAccessible=true
                val mPopup = fieldMPopup.get(popUpMenu)
                mPopup.javaClass
                    .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                    .invoke(mPopup, true)
            }catch (e:Exception){
                Log.e("Formulario", "Error showing menu icons", e)
            }finally {
                popUpMenu!!.show()
            }








        })
        userBD.child("InformacionPersonal").addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    retrieveData()
                }else{
                    //AQUÍ PONGO EL TELÉFONO
                }



            }

            override fun onCancelled(error: DatabaseError) {}
        })
        Guardar.setOnClickListener{
            nombre=txtNombreUserPerf.text.toString()
            apellido=txtApellidoUserPerf.text.toString()
            apellidoM=txtApellidoMUserPerf.text.toString()
            ciudad=txtMunicipioUserPerf.text.toString()
            curp=txtCURPUser.text.toString()

            if(!TextUtils.isEmpty(nombre) && !TextUtils.isEmpty(apellido) && !TextUtils.isEmpty(apellidoM) && !TextUtils.isEmpty(ciudad)){
                if (curp.length==18){
                    val cortado1 = curp.subSequence(0, 4)
                    val cortado2 = curp.subSequence(4, 10)
                    val cortado3 = curp.subSequence(10, 16)
                    val cortado4 = curp.subSequence(16, 18)
                    if (cortado1.matches("^[a-zA-Z]*$".toRegex()) && cortado2.matches("[0-9]+".toRegex()) &&cortado3.matches("^[a-zA-Z]*$".toRegex())
                        && cortado4.matches("[0-9]+".toRegex())){
                        progressBar.setVisibility(View.VISIBLE)
                        InfoGuardar()
                    }else{
                        txtCURPUser.setError("ALGÚN MATCH")
                    }


                }else{
                    txtCURPUser.setError("CURP Incorrecta ")
                }

                /*else if(curp.length>18){
                    txtCURPUser.setError("CURP Incorrecta 2")
                }*/

            }else if(nombre.isEmpty()){
                txtNombreUserPerf.setError("Campo requerido")
            }
            else if(apellido.isEmpty()){
                txtApellidoUserPerf.setError("Campo requerido")
            }
            else if(apellido.isEmpty()){
                txtApellidoUserPerf.setError("Campo requerido")
            }
            else if(apellidoM.isEmpty()){
                txtApellidoMUserPerf.setError("Campo requerido")
            }
            else if(ciudad.isEmpty()){
                txtMunicipioUserPerf.setError("Campo requerido")
            }



        }



    }

    override fun onSupportNavigateUp(): Boolean {
         super.onBackPressed()
        finish()
        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)


        return true
    }

    private fun borrarImagen() {
                    userBD.child("ImagenPerfil").removeValue()
                    val storage = FirebaseStorage.getInstance()
                    val storageRef = link?.let { storage.getReferenceFromUrl(it) }
                    // imageRef = storageRef.child(link)
                    storageRef!!.delete().addOnSuccessListener {
                        Toast.makeText(baseContext, "Foto de perfil borrada con éxito", Toast.LENGTH_LONG).show()
                        boleanoImagen=false
                    }.addOnFailureListener { exception ->
                       // Toast.makeText(baseContext, "FALLÓ!", Toast.LENGTH_LONG).show()
                    }
    }


    private fun retrieveFoto() {


        userBD.child("ImagenPerfil").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    boleanoImagen=true
                    //Toast.makeText(baseContext, "YA ENTRÓ A FOTO", Toast.LENGTH_LONG).show()
                    link=snapshot.value.toString()

                    //Picasso.get().load(link).into(imagen)

                   Picasso.get()
                        .load(link)
                        .into(imagen)

                            /*object: Target {
                        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

                        }

                        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {

                        }

                        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                            val ancho= bitmap!!.width
                            val alto=bitmap.height
                            if (alto>ancho){
                                Toast.makeText(baseContext, " NO ROTO", Toast.LENGTH_LONG).show()
                                Picasso.get().load(link)
                                    .error(R.drawable.camara_perfil)
                                    .into(imagen)
                            }else{
                                Toast.makeText(baseContext, " ROTO", Toast.LENGTH_LONG).show()
                                Picasso.get().load(link).rotate(90f)
                                    .error(R.drawable.camara_perfil)
                                    .into(imagen)
                            }
                        }

                    })*/





                }else{
                    imagen.setImageResource(R.drawable.camara_perfil)
                }



            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }


    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 502 && resultCode== Activity.RESULT_OK) {

            val uri = data?.data
            imagen.setImageURI(uri)

            //progressBar.setVisibility(View.VISIBLE)
            viewDialog.showDialog()

            val filename= UUID.randomUUID().toString()
            val ref= FirebaseStorage.getInstance().getReference("/imagesPerfil/$filename")

            ref.putFile(uri!!)
                .addOnSuccessListener {

                    ref.downloadUrl.addOnSuccessListener {
                        userBD.child("ImagenPerfil").setValue(it.toString())
                        //Toast.makeText(baseContext, ""+it.toString(), Toast.LENGTH_LONG).show()


                    }
                        .addOnCompleteListener({
                            viewDialog.hideDialog()
                        })
                   // progressBar.setVisibility(View.GONE)
                }
                .addOnCanceledListener {
                }

        }


        if (requestCode == 5892) {
            if (resultCode == Activity.RESULT_OK) {
                //progressBar.visibility=View.VISIBLE
                try {
                    //Toast.makeText(this, "imagen"+imageUri.toString(), Toast.LENGTH_LONG).show()
                    getRealPathFromURI(imageUri!!);


                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

        }
    }


    @SuppressLint("InlinedApi")
    private fun getRealPathFromURI(imageUri: Uri?) {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = managedQuery(imageUri, proj, null, null, null)
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        /*return*/ val esto=cursor.getString(column_index)

        val path = MediaStore.Images.Media.insertImage(contentResolver, esto, "Title", null)


      /*  val bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), path.toUri())
        val ancho= bitmap.width
        val alto=bitmap.height

        if (alto>ancho){
            Toast.makeText(this, "ES NORMAL Y ENTRA A NO ROTAR", Toast.LENGTH_LONG).show()
            if (path != null){

                val filename= UUID.randomUUID().toString()
                val ref= FirebaseStorage.getInstance().getReference("/imagesPerfil/$filename")

                ref.putFile(path.toUri())
                    .addOnSuccessListener {

                        ref.downloadUrl.addOnSuccessListener {
                            userBD.child("ImagenPerfil").setValue(it.toString())
                            Toast.makeText(baseContext, ""+it.toString(), Toast.LENGTH_LONG).show()


                        }
                        progressBar.setVisibility(View.GONE)
                    }
                    .addOnCanceledListener {
                    }

            }else{
                Toast.makeText(this, "null", Toast.LENGTH_LONG).show()
            }

        }else{
            Toast.makeText(this, "ENTRA A ROTAR", Toast.LENGTH_LONG).show()
            val matrix = Matrix();
            matrix.postRotate(90f);
            val bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            val os=this.getContentResolver().openOutputStream(path.toUri());
            bmp.compress(Bitmap.CompressFormat.PNG,100,os);*/

           // val pathito = MediaStore.Images.Media.insertImage(this.contentResolver, bmp, "Title", null)
            if (path != null){

                imagen.setImageURI(path.toUri())

                val filename= UUID.randomUUID().toString()
                val ref= FirebaseStorage.getInstance().getReference("/imagesPerfil/$filename")

                ref.putFile(path.toUri())
                    .addOnSuccessListener {

                        ref.downloadUrl.addOnSuccessListener {
                            userBD.child("ImagenPerfil").setValue(it.toString())
                            //Toast.makeText(baseContext, ""+it.toString(), Toast.LENGTH_LONG).show()

                        }
                        progressBar.setVisibility(View.GONE)
                    }
                    .addOnCanceledListener {
                    }

          /*  }else{
                Toast.makeText(this, "null", Toast.LENGTH_LONG).show()
            }*/

        }



    }




    private fun InfoGuardar() {
        /*//var progressValue = 0
       val timer = object: CountDownTimer(11000, 1000) {
           override fun onTick(millisUntilFinished: Long){
                //progressValue+=10
                //progressBar.setProgress(progressValue)
                if(progressValue>=100){
                    //progressBar.setVisibility(View.INVISIBLE)
                    viewDialog.showDialog()

                    val hMap:HashMap<String, String> = HashMap()
                    hMap.put("Nombre", nombre)
                    hMap.put("Apellido Paterno", apellido)
                    hMap.put("Apellido Materno",apellidoM)
                    hMap.put("Ciudad", ciudad)
                    hMap.put("Curp",curp)

                    userBD.child("InformacionPersonal").setValue(hMap)

                    Toast.makeText(baseContext, "Información actualizada", Toast.LENGTH_SHORT).show()


                }
            }
            override fun onFinish() {
                //progressBar.setVisibility(View.INVISIBLE)
                viewDialog.hideDialog()
            }
        }
        timer.start()*/
        viewDialog.showDialog()
        val handler = Handler()
        val hMap:HashMap<String, String> = HashMap()
        hMap.put("Nombre", nombre)
        hMap.put("Apellido Paterno", apellido)
        hMap.put("Apellido Materno",apellidoM)
        hMap.put("Ciudad", ciudad)
        hMap.put("Curp",curp)

        userBD.child("InformacionPersonal").setValue(hMap).addOnCompleteListener({
            Toast.makeText(baseContext, "Información actualizada", Toast.LENGTH_SHORT).show()
        })
        handler.postDelayed({
            viewDialog.hideDialog()
        }, 5000)

    }

    private fun retrieveData() {
        userBD.child("InformacionPersonal").child("Nombre").addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    val nombreU:String= snapshot.value.toString()
                    if (nombreU!=""){
                        txtNombreUserPerf.setText(nombreU)
                    }

                }else{

                }



            }

            override fun onCancelled(error: DatabaseError) {}
        })
        userBD.child("InformacionPersonal").child("Apellido Paterno").addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    val ApellidoU:String= snapshot.value.toString()
                    if (ApellidoU!=""){
                        txtApellidoUserPerf.setText(ApellidoU)
                    }

                }else{

                }



            }

            override fun onCancelled(error: DatabaseError) {}
        })
        userBD.child("InformacionPersonal").child("Apellido Materno").addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    val ApellidoM:String= snapshot.value.toString()
                    if (ApellidoM!=""){
                        txtApellidoMUserPerf.setText(ApellidoM)
                    }

                }else{

                }



            }

            override fun onCancelled(error: DatabaseError) {}
        })
        userBD.child("InformacionPersonal").child("Ciudad").addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    val CiudadU:String= snapshot.value.toString()
                    if (CiudadU!=""){
                        txtMunicipioUserPerf.setText(CiudadU)
                    }

                }else{

                }



            }

            override fun onCancelled(error: DatabaseError) {}
        })
        userBD.child("InformacionPersonal").child("Curp").addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    val CURP:String= snapshot.value.toString()
                    if (CURP!=""){
                        txtCURPUser.setText(CURP)
                    }

                }else{

                }



            }

            override fun onCancelled(error: DatabaseError) {}
        })


    }

    override fun onBackPressed() {

            super.onBackPressed()
            finish()
            overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
        }
        return super.onOptionsItemSelected(item);
    }
}
