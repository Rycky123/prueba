package com.fge.fge.Models

import android.app.Application
import android.net.Uri

class GlobalClass : Application() {

  var spinner: String? = null
  var narrativa: String? = null
  var ubicacion:String? = null
  var numero: String? = null
  var calle: String? = null
  var colonia: String? = null
  var ciudad: String? = null
  var estado: String? = null
  var pais: String? = null
  var cp: String? = null
  var audio: String? = null
  var audio2: String? = null
  var imagenes: ArrayList<String>? = null
  var contador:Int?=null
  var videoSlected: String? = null
  var latitud: Double? = null
  var longitud: Double? = null

}