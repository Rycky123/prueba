package com.fge.fge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_acerca.*


class AcercaDeActivity : AppCompatActivity() {
    lateinit var terminos:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acerca)

       val toolbar = findViewById<Toolbar>(R.id.toolbarAcerca)
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        terminos=findViewById(R.id.terminosText)

        versionApp.setText(BuildConfig.VERSION_NAME)

        terminos.setOnClickListener({
            val intento1 = Intent(this, TerminosCondicionesActivity::class.java)
            startActivity(intento1)
        })

        /*val versionCode = BuildConfig.VERSION_CODE
        val versionName = BuildConfig.VERSION_NAME*/

       // Toast.makeText(this, "Versión: $versionCode, y nombre: $versionName", Toast.LENGTH_SHORT).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        super.onBackPressed()
        finish()
        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
        return true
    }
}
