package com.fge.fge

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_session.*
import kotlinx.android.synthetic.main.boton_llamada.view.*
import kotlinx.android.synthetic.main.terminos_condiciones.view.*
import java.util.concurrent.TimeUnit
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.preference.PreferenceManager
import android.util.Log
import android.view.inputmethod.EditorInfo
import java.util.*


class SessionActivity : AppCompatActivity() {
    lateinit var mCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    var mAuth: FirebaseAuth? = null
    var verificationId = ""
    var phnNo = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session)

        mAuth = FirebaseAuth.getInstance()

        btnEnviar.setOnClickListener {
            numeroTelefono.onEditorAction(EditorInfo.IME_ACTION_DONE);
            val numerito2 = numeroTelefono.text.trim().toString()
            if (!numerito2.isEmpty() && numerito2.length == 10) {
                val prefManager2 = PreferenceManager.getDefaultSharedPreferences(this)
                if (!prefManager2.getBoolean("buabuab", false)) {
                    //toast("MUESTRO LOS TÉRMINOS")
                    val mDialogView =
                        LayoutInflater.from(this).inflate(R.layout.terminos_condiciones, null)
                    val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
                    val mAlertDialog = mBuilder.show()
                    mAlertDialog.setCancelable(false)
                    //mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    mDialogView.declinar.setOnClickListener {
                        mAlertDialog.dismiss()
                    }
                    mDialogView.aceptar.setOnClickListener {
                        verify()
                        progressBar.visibility = View.VISIBLE
                        val prefEditor = prefManager2.edit()
                        prefEditor.putBoolean("buabuab", true)
                        prefEditor.apply()
                        //aqui inhabilito el botón

                        mAlertDialog.dismiss()
                    }
                    mDialogView.terminosCompletos.setOnClickListener({
                        startActivity(Intent(this, TerminosCondicionesActivity::class.java))
                    })
                } else {
                    verify()
                    progressBar.visibility = View.VISIBLE
                    toast("YA HAY ALGO GUARDADO, POR LO TANTO NO MUESTRO LOS TÉRMINOS")
                }


            } else {
                numeroTelefono.setError("Número inválido")

            }


        }

    }


    private fun verify() {
        verificationCallbacks()

        phnNo = "+52" + numeroTelefono.text.toString()


        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phnNo,
            60,
            TimeUnit.SECONDS,
            this,
            mCallbacks
        )
    }

    private fun verificationCallbacks() {
        mCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                progressBar.visibility = View.INVISIBLE
                signIn(credential)
            }

            override fun onVerificationFailed(p0: FirebaseException) {
                toast("Error de servicio, intentar más tarde")
                progressBar.visibility = View.INVISIBLE
                //aqui habilito de nuevo el botón
            }

            override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
                super.onCodeSent(p0, p1)
                verificationId = p0.toString()
                progressBar.visibility = View.INVISIBLE
                toast("Espere un momento por favor")

            }
        }
    }

    private fun signIn(credential: PhoneAuthCredential) {
        mAuth?.signInWithCredential(credential)
            ?.addOnCompleteListener { task: Task<AuthResult> ->
                if (task.isSuccessful) {
                    //userBD.child("Numero").setValue(phnNo)
                    //toast("AQUI YA HABRÍA ENTRADO")
                    startActivity(Intent(this, InicioActivity::class.java))
                    finish()
                }
            }
    }

    private fun toast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

}
