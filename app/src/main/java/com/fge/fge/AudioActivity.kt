package com.fge.fge

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.skyfishjy.library.RippleBackground
import kotlinx.android.synthetic.main.activity_audio.*
import java.lang.Exception
import android.os.CountDownTimer
import android.preference.PreferenceManager
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.fge.fge.Models.GlobalClass
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.getkeepsafe.taptargetview.TapTargetView
import kotlinx.android.synthetic.main.activity_audio.adjuntarAudio
import kotlinx.android.synthetic.main.activity_audio.cancelar
import kotlinx.android.synthetic.main.activity_audio.volver
import kotlinx.android.synthetic.main.activity_detalles.*
import kotlinx.android.synthetic.main.activity_multimedia.*
import kotlinx.android.synthetic.main.cancelar_predenuncia.view.*
import kotlinx.android.synthetic.main.cancelar_predenuncia.view.siAlert
import kotlinx.android.synthetic.main.confirmar_audio.view.*
import java.io.*
import java.text.DecimalFormat
import java.util.*


class AudioActivity : AppCompatActivity() {
    private var mPlayerito:MediaPlayer?=null
    override fun onStart() {
        super.onStart()
        val globalVariable = applicationContext as GlobalClass

        val audio=globalVariable.audio
        if (audio != null){
            //Toast.makeText(this, "AUDIO 1 LLENO", Toast.LENGTH_SHORT).show()
            grabado.visibility=View.VISIBLE
            adjuntado.visibility=View.GONE
            borrar.visibility=View.VISIBLE
            //Toast.makeText(this, "Hola, está lleno, ya has pasado por aquí", Toast.LENGTH_SHORT).show()
            btnPlay2.visibility=View.VISIBLE
            playbtn!!.visibility=View.GONE
            stopbtn!!.isEnabled = false
            startbtn!!.isEnabled = true

            borrar.isEnabled=true

            btnPlay2.setOnClickListener({
                mPlayerito= MediaPlayer()
                try {
                    mPlayerito!!.setDataSource(audio)
                    mPlayerito!!.prepare()
                    mPlayerito!!.start()
                    mPlayerito!!.setOnCompletionListener {
                        esto!!.stopRippleAnimation()
                        btnPlay2!!.isEnabled=true
                    }

                    mPlayerito!!.setOnCompletionListener({
                        btnPlay2!!.isEnabled=true
                        esto!!.stopRippleAnimation()
                    })

                    tiempo.visibility=View.VISIBLE
                    chronometer.visibility=View.GONE
                    val hola=mPlayerito!!.duration

                    yourCountDownTimer = object : CountDownTimer(hola.toLong(), 1000) {

                        override fun onTick(millisUntilFinished: Long) {

                            val f = DecimalFormat("00")
                           // val hour = millisUntilFinished / 3600000 % 24
                            val min = millisUntilFinished / 60000 % 60
                            val sec = millisUntilFinished / 1000 % 60

                            tiempo.setText(/*f.format(hour) + ":" + */f.format(min) + ":" + f.format(sec))
                            isRunning=true
                        }

                        override fun onFinish() {
                            tiempo.setText("00:00")
                            isRunning=false
                        }
                    }.start()

                } catch (e: IOException) {
                    Log.e(LOG_TAG, "prepare() failed")
                }

                esto!!.startRippleAnimation()


            })

        }

        val audio2=globalVariable.audio2
        if (audio2!=null){
            adjuntado.visibility=View.VISIBLE
            grabado.visibility=View.GONE
            borrar2.visibility=View.VISIBLE
            Toast.makeText(this, "AUDIO 2 LLENO", Toast.LENGTH_SHORT).show()
            btnPlay2Adjuntado.visibility=View.VISIBLE
            btnPlayAdjuntado.visibility=View.GONE
            btnPlay2Adjuntado.isEnabled=true
            btnStopAdjuntado.isEnabled=false
            btnPlay2Adjuntado!!.setImageResource(R.drawable.play)
            btnStopAdjuntado!!.setImageResource(R.drawable.stop_gris)
            val tratar=MediaPlayer()
            try{
                audioFileUri?.let { tratar.setDataSource(this, it.toUri()) }
                tratar.prepare()//No puedes obtener la duración hasta que fuckin leas los datos
                val duracio= tratar.duration.toLong()
               // Toast.makeText(applicationContext, ""+duracio.toString(), Toast.LENGTH_LONG).show()
                if (duracio>900000) {
                    val builder = AlertDialog.Builder(this@AudioActivity)
                    builder.setTitle("AUDIO MUY GRANDE")
                    builder.setMessage("¿Desea elegír otro audio?")
                    builder.setPositiveButton("Sí"){dialog, which ->
                        globalVariable.audio2=null
                        val intent: Intent
                        intent = Intent()
                        intent.action = Intent.ACTION_GET_CONTENT
                        intent.type = "audio/*"
                        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_audio_file_title)), 1289)

                    }
                    builder.setNegativeButton("No"){dialog,which ->

                        globalVariable.audio2=null
                        tiempo.visibility=View.GONE
                        adjuntado.visibility=View.GONE
                        grabado.visibility=View.VISIBLE
                        dialog.dismiss()
                    }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }

                } catch (e:Exception){

            }



            btnPlay2Adjuntado.setOnClickListener({
                mPlayerito = MediaPlayer()
                try {
                    Toast.makeText(applicationContext, "REPRODUCIENDOOOO", Toast.LENGTH_LONG).show()
                    mPlayerito!!.setDataSource(this, audio2.toUri())
                    mPlayerito!!.prepare()
                    mPlayerito!!.start()

                    mPlayerito!!.setOnCompletionListener({
                        btnStopAdjuntado.isEnabled=false
                        btnStopAdjuntado!!.setImageResource(R.drawable.stop_gris)
                        btnPlay2Adjuntado.isEnabled=true
                        btnPlay2Adjuntado!!.setImageResource(R.drawable.play)
                        esto!!.stopRippleAnimation()
                    })

                    if (mPlayerito!!.isPlaying){
                        btnStopAdjuntado.isEnabled=true
                        btnStopAdjuntado!!.setImageResource(R.drawable.stop)
                    }


                 /*  mPlayerito!!.setOnInfoListener(MediaPlayer.OnInfoListener { mp, what, extra ->
                        if ( mPlayerito!!.duration.toLong()>900000) {
                            mPlayerito!!.stop()
                            esto!!.stopRippleAnimation()
                            yourCountDownTimer!!.cancel()
                            val builder = AlertDialog.Builder(this@AudioActivity)
                            builder.setTitle("AUDIO MUY GRANDE")
                            builder.setMessage("¿Desea elegír otro audio?")
                            builder.setPositiveButton("Sí"){dialog, which ->
                                globalVariable.audio2=null
                                val intent: Intent
                                intent = Intent()
                                intent.action = Intent.ACTION_GET_CONTENT
                                intent.type = "audio/*"
                                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_audio_file_title)), 1289)

                            }
                            builder.setNegativeButton("No"){dialog,which ->
                                globalVariable.audio2=null
                                tiempo.visibility=View.GONE
                                adjuntado.visibility=View.GONE
                                grabado.visibility=View.VISIBLE
                                dialog.dismiss()
                            }
                            val dialog: AlertDialog = builder.create()
                            dialog.show()
                            return@OnInfoListener true
                        }
                        false
                    })*/*/



                    tiempo.visibility=View.VISIBLE
                    chronometer.visibility=View.GONE
                    val hola=mPlayerito!!.duration

                    yourCountDownTimer = object : CountDownTimer(hola.toLong(), 1000) {

                        override fun onTick(millisUntilFinished: Long) {

                            val f = DecimalFormat("00")
                            //val hour = millisUntilFinished / 3600000 % 24
                            val min = millisUntilFinished / 60000 % 60
                            val sec = millisUntilFinished / 1000 % 60

                            tiempo.setText(/*f.format(hour) + ":" + */f.format(min) + ":" + f.format(sec))
                            isRunning=true
                        }

                        override fun onFinish() {
                            tiempo.setText("00:00")
                            isRunning=false
                        }
                    }.start()

                } catch (e: IOException) {
                    Toast.makeText(applicationContext, "fALLÓOOOOO", Toast.LENGTH_LONG).show()
                }

                esto!!.startRippleAnimation()

                btnPlay2Adjuntado.isEnabled=false
                btnPlay2Adjuntado!!.setImageResource(R.drawable.play_gris)

            })
        }
        if(audio==null && audio2==null){
            grabado.visibility=View.VISIBLE
            adjuntado.visibility=View.GONE
            //Toast.makeText(this, "NINGUNO LLENO", Toast.LENGTH_SHORT).show()
            chronometer.visibility=View.VISIBLE
        }

    }
    //Grabar audio
    private var startbtn: ImageButton? = null
    private var stopbtn: ImageButton? = null
    private var playbtn: ImageButton? = null

    private var mRecorder: MediaRecorder? = null
    private var mPlayer: MediaPlayer? = null
    private var esto: RippleBackground? = null

    var timeWhenStopped = 0L
    var contador:Int=0

    var yourCountDownTimer:CountDownTimer?=null
    var isRunning=false

    var array=ArrayList<String>()


    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio)

        val b = this.intent.extras

        if(b != null){
            if (b.containsKey("HOLA")){
                array = b.getStringArrayList("HOLA") as ArrayList<String>
                Toast.makeText(baseContext, "lleno: "+array.size, Toast.LENGTH_SHORT).show()
                // print("lleno"+array)
            }
        }else{
            Toast.makeText(baseContext, "ARRAY VACIO", Toast.LENGTH_SHORT).show()
        }

        mostrarGuia()
        //grabar audio botones
        startbtn = findViewById<View>(R.id.btnRecord) as ImageButton
        stopbtn = findViewById<View>(R.id.btnStop) as ImageButton
        playbtn = findViewById<View>(R.id.btnPlay) as ImageButton
        /*mFileName=Environment.getExternalStorageDirectory()
            .getPath()+"/"+ UUID.randomUUID().toString()+".3gp"*/

        val dir=Environment.getExternalStorageDirectory().getPath()+ "/" +"Evidencia Fiscalia"+"/"+"Audios"+"/"
        val file=File(dir)
        if (!file.exists()){
            file.mkdirs();
        }

        mFileName=Environment.getExternalStorageDirectory().getPath()+ "/" +"Evidencia Fiscalia"+"/"+"Audios"+"/"+ "audioGrabado"+".3gp"
        audioFileUri=Environment.getExternalStorageDirectory()
            .getPath()+"/"+ UUID.randomUUID().toString()+".3gp"

        val globalVariable = applicationContext as GlobalClass

        //////////////////////////////////////////////////////Cancelar predenuncia hasta éste punto/////////////////////
        cancelar.setOnClickListener({
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.cancelar_predenuncia, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val  mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.seguirAlert.setOnClickListener {

                mAlertDialog.dismiss()
            }
            mDialogView.siAlert.setOnClickListener {
                val globalVariable = applicationContext as GlobalClass
                globalVariable.spinner=null
                globalVariable.narrativa=null
                globalVariable.ubicacion=null
                globalVariable.numero=null
                globalVariable.calle=null
                globalVariable.colonia=null
                globalVariable.ciudad=null
                globalVariable.estado=null
                globalVariable.pais=null
                globalVariable.cp=null
                globalVariable.audio=null
                globalVariable.audio2=null
                globalVariable.imagenes=null
                globalVariable.contador=null
                globalVariable.videoSlected=null
                globalVariable.latitud=null
                globalVariable.longitud=null
                val i = Intent(baseContext, InicioActivity::class.java)
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(i)
                finish()

            }



        })
        //////////////////////////////////////////////////////VARIABLES INICIALES PARA AUDIO 1//////////////////////////
        if (grabado.visibility==View.VISIBLE){
            stopbtn!!.isEnabled=false
            stopbtn!!.setImageResource(R.drawable.stop_gris)
            playbtn!!.isEnabled=false
            playbtn!!.setImageResource(R.drawable.play_gris)

        }
        if (adjuntado.visibility==View.VISIBLE){
            btnStopAdjuntado.isEnabled=false
            btnStopAdjuntado!!.setImageResource(R.drawable.stop_gris)
            btnPlayAdjuntado.isEnabled=false
            btnPlayAdjuntado!!.setImageResource(R.drawable.play_gris)

        }
        esto=findViewById(R.id.content)

        /////////////////////////////////////////////////BORRAR AUDIOS//////////////////////////////////////////////////
        borrar.setOnClickListener({
            if (mPlayer != null) {
                mPlayer!!.reset();
                mPlayer!!.release();
                mPlayer= null;
                esto!!.stopRippleAnimation()
            }
            if(mPlayerito !=null){
                mPlayerito!!.reset()
                mPlayerito!!.release()
                mPlayerito=null
                esto!!.stopRippleAnimation()

            }
            if (globalVariable.audio!=null){
                globalVariable.audio=null
               // Toast.makeText(this, "Sí!", Toast.LENGTH_SHORT).show()
                resetear()

            }
            if (globalVariable.audio2!=null){
                globalVariable.audio2=null
                //Toast.makeText(this, "Sí!", Toast.LENGTH_SHORT).show()
                resetear()
            }

            if (isRunning==true){
                yourCountDownTimer!!.cancel()
            }
            chronometer.visibility=View.VISIBLE
            tiempo.visibility=View.GONE
           //esto
            if (borrar.visibility==View.VISIBLE){
                borrar.visibility=View.GONE
                playbtn!!.visibility=View.VISIBLE
                btnPlay2.visibility=View.GONE
            }
            //
            playbtn!!.isEnabled=false
            playbtn!!.setImageResource(R.drawable.play_gris)
            stopbtn!!.isEnabled=false
            borrar.visibility=View.GONE
            if (btnPlay2.getVisibility()==View.VISIBLE){
                btnPlay2.visibility=View.GONE
                playbtn!!.visibility=View.VISIBLE
            }
        })
        borrar2.setOnClickListener({
            if (mPlayer != null) {
                mPlayer!!.reset();
                mPlayer!!.release();
                mPlayer= null;
                esto!!.stopRippleAnimation()
            }
            if(mPlayerito !=null){
                mPlayerito!!.reset()
                mPlayerito!!.release()
                mPlayerito=null
                esto!!.stopRippleAnimation()

            }
            if (globalVariable.audio!=null){
                globalVariable.audio=null
                //Toast.makeText(this, "Sí!", Toast.LENGTH_SHORT).show()
                resetear()

            }
            if (globalVariable.audio2!=null){
                globalVariable.audio2=null
                //Toast.makeText(this, "Sí!", Toast.LENGTH_SHORT).show()
                resetear()

            }

            //borrar el audio en la carpeta?

            if (isRunning==true){
                yourCountDownTimer!!.cancel()
            }
            chronometer.visibility=View.VISIBLE
            tiempo.visibility=View.GONE

            btnPlay2Adjuntado.isEnabled=false
            btnPlay2Adjuntado!!.setImageResource(R.drawable.play_gris)
            btnStopAdjuntado.isEnabled=false
            btnStopAdjuntado!!.setImageResource(R.drawable.stop_gris)
            borrar2.visibility=View.GONE

            esto!!.stopRippleAnimation()

            adjuntarAudio2.visibility=View.VISIBLE

            mostrarGuia4()
        })
        /////////////////////////////////////////////////AUDIO UNO//////////////////////////////////////////////////////
        startbtn!!.setOnClickListener {
            try{
                mPlayer!!.release()
                mPlayer = null

            }catch (e: Exception){

            }
            iniciarGrabar()
            chronometer.visibility=View.VISIBLE
            tiempo.visibility=View.GONE
            contador++
            if (contador==1){
                resetear()
                chronometer.base = SystemClock.elapsedRealtime() + timeWhenStopped
                chronometer.start()
            }else if(contador>1){
                contador=0
                resetear()
                chronometer.base = SystemClock.elapsedRealtime() + timeWhenStopped
                chronometer.start()
            }
        }
        stopbtn!!.setOnClickListener {
            borrar.visibility=View.VISIBLE
            esto!!.stopRippleAnimation()
            pararGrabar()
            timeWhenStopped = chronometer.base - SystemClock.elapsedRealtime()
            chronometer.stop()
            globalVariable.audio= mFileName
            //siguienteActividad3!!.isEnabled=true


        }
        playbtn!!.setOnClickListener {
            stopbtn!!.isEnabled = false
            stopbtn!!.setImageResource(R.drawable.stop_gris)
            startbtn!!.isEnabled = true
            startbtn!!.setImageResource(R.drawable.grabar)
            playbtn!!.isEnabled = false
            playbtn!!.setImageResource(R.drawable.play)

            mPlayer = MediaPlayer()
            try {
                mPlayer!!.setDataSource(mFileName)
                mPlayer!!.prepare()
                mPlayer!!.start()
                mPlayer!!.setOnCompletionListener {
                    esto!!.stopRippleAnimation()
                    playbtn!!.isEnabled=true
                }

                mPlayer!!.setOnCompletionListener({
                    playbtn!!.isEnabled=true
                    esto!!.stopRippleAnimation()
                })

                val hola=mPlayer!!.duration

                chronometer.visibility=View.GONE
                tiempo.visibility=View.VISIBLE

                yourCountDownTimer = object : CountDownTimer(hola.toLong(), 1000) {

                    override fun onTick(millisUntilFinished: Long) {

                        val f = DecimalFormat("00")
                        //val hour = millisUntilFinished / 3600000 % 24
                        val min = millisUntilFinished / 60000 % 60
                        val sec = millisUntilFinished / 1000 % 60

                        isRunning = true

                        tiempo.setText(/*f.format(hour) + ":" +*/ f.format(min) + ":" + f.format(sec))
                    }

                    override fun onFinish() {
                        tiempo.setText("00:00")

                        isRunning = false
                    }
                }.start()



            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }

            esto!!.startRippleAnimation()
        }

        //////////////////////////////////////////////////CAMBIO DE UNTERFACES//////////////////////////////////////////
        grabarAudioVolver.setOnClickListener({

            if(mPlayer !=null){
                mPlayer!!.reset()
                mPlayer!!.release()
                mPlayer=null
            }
            if(mPlayerito !=null){
                mPlayerito!!.reset()
                mPlayerito!!.release()
                mPlayerito=null
            }
            if(mRecorder !=null){
                mRecorder!!.reset()
                mRecorder!!.release()
                mRecorder=null
            }else{
            }
            val audioAdjuntado=globalVariable.audio2
            if (audioAdjuntado!=null){



                val mDialogView = LayoutInflater.from(this).inflate(R.layout.confirmar_audio, null)
                val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
                val  mAlertDialog = mBuilder.show()
                mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                mDialogView.noSeguirAlertAudio.setOnClickListener {

                    mAlertDialog.dismiss()
                }
                mDialogView.siAlertAudio.setOnClickListener {
                    mAlertDialog.dismiss()
                    adjuntado.visibility=View.GONE
                    grabado.visibility=View.VISIBLE
                    resetear()
                    tiempo.visibility=View.GONE
                    chronometer.visibility=View.VISIBLE
                    if(audioFileUri!=null){
                        audioFileUri=null
                    }


                    globalVariable.audio2=null
                    if (isRunning==true){
                        yourCountDownTimer!!.cancel()

                    }

                    if (esto!!.isRippleAnimationRunning){
                        esto!!.stopRippleAnimation()
                    }


                }




            }else{
                tiempo.visibility=View.GONE
                chronometer.visibility=View.VISIBLE
                adjuntado.visibility=View.GONE
                grabado.visibility=View.VISIBLE
                globalVariable.audio=null
                resetear()

                if (isRunning==true){
                    yourCountDownTimer!!.cancel()
                }
            }



        })
        adjuntarAudio.setOnClickListener({
            adjuntarAudioMetodo()
        })
        adjuntarAudio2.setOnClickListener({
            adjuntarAudioMetodo()

        })
        //////////////////////////////////////////////////MOVIMIENTO DE ACTIVIDADES/////////////////////////////////////
        siguienteActividad3.setOnClickListener {
            if (mPlayer != null) {
                mPlayer!!.reset();
                mPlayer!!.release();
                mPlayer= null;
                //Fredo!!.cancel()
                esto!!.stopRippleAnimation()
            }
            if(mPlayerito !=null){
                mPlayerito!!.reset()
                mPlayerito!!.release()
                mPlayerito=null
                esto!!.stopRippleAnimation()
            }
            if(mRecorder !=null){
                mRecorder!!.reset()
                mRecorder!!.release()
                mRecorder=null
            }

            if (esto!!.isRippleAnimationRunning){
                esto!!.stopRippleAnimation()
            }

            if (isRunning){
                yourCountDownTimer!!.cancel()
            }

            timeWhenStopped = chronometer.base - SystemClock.elapsedRealtime()
            chronometer.stop()
            resetear()

            if(array.isEmpty()){
                val i = Intent(baseContext, MultimediaActivity::class.java)
                startActivity(i)
            }else{
                val be = Bundle()
                be.putStringArrayList("HOLA2", array)
                val intent = Intent(this, MultimediaActivity::class.java)
                intent.putExtras(be)
                startActivity(intent)
            }

        }
        volver.setOnClickListener({
            if (mPlayer != null) {
                mPlayer!!.reset();
                mPlayer!!.release();
                mPlayer= null;
                //Fredo!!.cancel()
                esto!!.stopRippleAnimation()
            }
            if(mPlayerito !=null){
                mPlayerito!!.reset()
                mPlayerito!!.release()
                mPlayerito=null
                esto!!.stopRippleAnimation()
            }
            if(mRecorder !=null){
                mRecorder!!.reset()
                mRecorder!!.release()
                mRecorder=null
            }else{
            }

            if (esto!!.isRippleAnimationRunning){
                esto!!.stopRippleAnimation()
            }

            if (isRunning){
                yourCountDownTimer!!.cancel()
            }
            resetear()
            finish()
            overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
        })
        btnStopAdjuntado.setOnClickListener({
            mPlayerito!!.release()
            mPlayerito = null
            esto!!.stopRippleAnimation()
            yourCountDownTimer!!.cancel()

            btnPlay2Adjuntado.isEnabled=true
            btnPlay2Adjuntado!!.setImageResource(R.drawable.play)
            btnStopAdjuntado.isEnabled=false
            btnStopAdjuntado!!.setImageResource(R.drawable.stop_gris)

        })

    }

    private fun mostrarGuia4() {
        val prefManager7 = PreferenceManager.getDefaultSharedPreferences(this)
        if (!prefManager7.getBoolean("DSASJNLKJSA", false)){
            TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.adjuntarAudio2),
                "Volver a Adjuntar", "Adejuntar nuevo audio."
            )
                .cancelable(false)
                .drawShadow(true)
                .dimColor(R.color.Dorado)
                .outerCircleColor(R.color.colorAccent)
                .targetCircleColor(R.color.colorPrimaryDark)
                .tintTarget(false), object : TapTargetView.Listener() {
                override fun onTargetClick(view: TapTargetView) {
                    super.onTargetClick(view)
                    val prefEditor = prefManager7.edit()
                     prefEditor.putBoolean("DSASJNLKJSA", true)
                     prefEditor.apply()
                    //sequence.start()
                }

                override fun onOuterCircleClick(view: TapTargetView) {
                    super.onOuterCircleClick(view)
                    Toast.makeText(view.context, "tocaste fuera del circulo", Toast.LENGTH_SHORT).show()
                }

                override fun onTargetDismissed(view: TapTargetView, userInitiated: Boolean) {
                    Log.d("TapTargetViewSample", "You dismissed me :(")
                }

            })
        }else{
           // Toast.makeText(this, "Ya no muestro", Toast.LENGTH_LONG).show()
        }
    }

    private fun mostrarGuia2() {
        val prefManager5 = PreferenceManager.getDefaultSharedPreferences(this)
        if (!prefManager5.getBoolean("AHUAHAUAHA", false)){


            val sequence = TapTargetSequence(this)
                .targets(
                    // This tap target will target the back button, we just need to pass its containing toolbar
                    TapTarget.forView(findViewById(R.id.borrar2), "Eliminar Audio Adjuntado",
                        "Elimine.")
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.Dorado)
                        .id(1)
                )

            TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.grabarAudioVolver),
                "Volver a grabar", "Volver a la interfaz de grabadora."
            )
                .cancelable(false)
                .drawShadow(true)
                .dimColor(R.color.Dorado)
                .outerCircleColor(R.color.colorAccent)
                .targetCircleColor(R.color.colorPrimaryDark)
                .tintTarget(false), object : TapTargetView.Listener() {
                override fun onTargetClick(view: TapTargetView) {
                    super.onTargetClick(view)
                    val prefEditor = prefManager5.edit()
                     prefEditor.putBoolean("AHUAHAUAHA", true)
                     prefEditor.apply()
                    sequence.start()
                }

                override fun onOuterCircleClick(view: TapTargetView) {
                    super.onOuterCircleClick(view)
                    Toast.makeText(view.context, "tocaste fuera del circulo", Toast.LENGTH_SHORT).show()
                }

                override fun onTargetDismissed(view: TapTargetView, userInitiated: Boolean) {
                    Log.d("TapTargetViewSample", "You dismissed me :(")
                }

            })
        }else{
            //Toast.makeText(this, "Ya no muestro", Toast.LENGTH_LONG).show()
        }
    }

    private fun mostrarGuia() {
        val prefManager4 = PreferenceManager.getDefaultSharedPreferences(this)
        if (!prefManager4.getBoolean("AJUAAA", false)){


            val sequence = TapTargetSequence(this)
                .targets(
                    // This tap target will target the back button, we just need to pass its containing toolbar
                    TapTarget.forView(findViewById(R.id.btnStop), "Parar la grabación",
                        "Parará el proceso de grabado.")
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.colorAccent)
                        .id(1),
                    TapTarget.forView(findViewById(R.id.btnPlay), "Reproducir Narrativa",
                        "Podrá escuchar la narrativa.")
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.colorAccent)
                        .id(2),
                    TapTarget.forView(findViewById(R.id.adjuntarAudio), "Adjuntar Audio ya grabado",
                        "Podrá seleccionar audios desde su dispositivo.")
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.colorAccent)
                        .id(3),
                    TapTarget.forView(findViewById(R.id.volver), "Volver",
                        "Volver a la sección anterior para verificar datos.")
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.Dorado)
                        .id(4)
                )

            TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.btnRecord),
                "Grabar Narrativa", "Presiona para grabar."
            )
                .cancelable(false)
                .drawShadow(true)
                .dimColor(R.color.Dorado)
                .outerCircleColor(R.color.colorAccent)
                .targetCircleColor(R.color.colorPrimaryDark)
                .tintTarget(false), object : TapTargetView.Listener() {
                override fun onTargetClick(view: TapTargetView) {
                    super.onTargetClick(view)
                    val prefEditor = prefManager4.edit()
                     prefEditor.putBoolean("AJUAAA", true)
                     prefEditor.apply()
                    sequence.start()
                }

                override fun onOuterCircleClick(view: TapTargetView) {
                    super.onOuterCircleClick(view)
                    Toast.makeText(view.context, "tocaste fuera del circulo", Toast.LENGTH_SHORT).show()
                }

                override fun onTargetDismissed(view: TapTargetView, userInitiated: Boolean) {
                    Log.d("TapTargetViewSample", "You dismissed me :(")
                }

            })
        }else{
           // Toast.makeText(this, "Ya no muestro", Toast.LENGTH_LONG).show()
        }
    }

    private fun adjuntarAudioMetodo() {
        val globalVariable = applicationContext as GlobalClass
        if(mPlayer !=null){
            mPlayer!!.reset()
            mPlayer!!.release()
            mPlayer=null
        }
        if(mPlayerito !=null){
            mPlayerito!!.reset()
            mPlayerito!!.release()
            mPlayerito=null
        }
        if(mRecorder !=null){
            mRecorder!!.reset()
            mRecorder!!.release()
            mRecorder=null
        }else{
        }
        val audioGrabado=globalVariable.audio
        if (audioGrabado!=null){


            val mDialogView = LayoutInflater.from(this).inflate(R.layout.confirmar_audio, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val  mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.noSeguirAlertAudio.setOnClickListener {

                mAlertDialog.dismiss()
            }
            mDialogView.siAlertAudio.setOnClickListener {
                mAlertDialog.dismiss()

                adjuntado.visibility=View.VISIBLE
                grabado.visibility=View.GONE
                resetear()
                tiempo.visibility=View.GONE

                borrar.visibility=View.GONE


                globalVariable.audio=null

                if (isRunning==true){
                    yourCountDownTimer!!.cancel()

                }

                if (esto!!.isRippleAnimationRunning){
                    esto!!.stopRippleAnimation()
                }

                tiempo.visibility=View.GONE
                chronometer.visibility=View.VISIBLE

                if(mFileName!=null){
                    mFileName=null
                }

                val intent: Intent
                intent = Intent()
                intent.action = Intent.ACTION_GET_CONTENT
                intent.type = "audio/*"
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_audio_file_title)), 1289)
            }

          /*  val builder = AlertDialog.Builder(this@AudioActivity)
            builder.setTitle("Si desea adjuntar audio el material grabado se perderá")
            builder.setMessage("¿Desea continuar?")
            builder.setPositiveButton("Sí"){dialog, which ->

                adjuntado.visibility=View.VISIBLE
                grabado.visibility=View.GONE
                resetear()
                tiempo.visibility=View.GONE

                borrar.visibility=View.GONE


                globalVariable.audio=null

                if (isRunning==true){
                    yourCountDownTimer!!.cancel()

                }

                if (esto!!.isRippleAnimationRunning){
                    esto!!.stopRippleAnimation()
                }

                tiempo.visibility=View.GONE
                chronometer.visibility=View.VISIBLE

                if(mFileName!=null){
                    mFileName=null
                }*/

               /* val intent: Intent
                intent = Intent()
                intent.action = Intent.ACTION_GET_CONTENT
                intent.type = "audio/*"
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_audio_file_title)), 1289)*/*/

           /* }
            builder.setNegativeButton("No"){dialog,which ->
                dialog.dismiss()
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()

        }else{
            adjuntado.visibility=View.VISIBLE
            grabado.visibility=View.GONE
            globalVariable.audio=null
            resetear()

            if (mPlayer != null) {
                mPlayer!!.reset();
                mPlayer!!.release();
                mPlayer= null;
                esto!!.stopRippleAnimation()
            }
            if (mPlayerito != null) {
                mPlayerito!!.reset();
                mPlayerito!!.release();
                mPlayerito= null;
                esto!!.stopRippleAnimation()
            }

            if (isRunning==true){
                yourCountDownTimer!!.cancel()
            }
            if (esto!!.isRippleAnimationRunning){
                esto!!.stopRippleAnimation()
            }

            val intent: Intent
            intent = Intent()
            intent.action = Intent.ACTION_GET_CONTENT
            intent.type = "audio/*"
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_audio_file_title)), 1289)
        }*/*/


    }else{
            val intent: Intent
            intent = Intent()
            intent.action = Intent.ACTION_GET_CONTENT
            intent.type = "audio/*"
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_audio_file_title)), 1289)
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1289 && resultCode == Activity.RESULT_OK){
            if ((data != null) && (data.getData() != null)){
                audioFileUri = data.getData().toString()
                // val listo=audioFileUri.path

                if (adjuntarAudio2.visibility==View.VISIBLE){
                    adjuntarAudio2.visibility=View.GONE
                }
                mostrarGuia2()

                val globalVariable = applicationContext as GlobalClass
                //globalVariable.audio2= //la variable de newfile
                try {

                    var newfile:File?=null

                    val videoAsset = getContentResolver().openAssetFileDescriptor(data.getData()!!, "r");
                    val init = videoAsset!!.createInputStream();

                    val filepath = Environment.getExternalStorageDirectory();
                    val dir = File(filepath.getAbsolutePath() + "/" +"Evidencia Fiscalia" + "/"+"/"+"Audios");
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }

                    newfile = File(dir, "audio"+/*System.currentTimeMillis()+*/".mp3");

                    if (newfile.exists()) newfile.delete();



                    val out = FileOutputStream(newfile);

                    val buffer = ByteArray(1024)
                    var length = init.read(buffer)

                    //Transferring data
                    while(length != -1) {
                        out.write(buffer, 0, length)
                        length = init.read(buffer)
                    }

                    //Finalizing
                    out.flush()
                    out.close()
                    out.close()
                    //Toast.makeText(this, "éxito total", Toast.LENGTH_SHORT).show()

                    globalVariable.audio2=newfile.toString()

                    /*videoView!!.setVideoURI(selectedImageUri)
                    videoView!!.start()*/

                } catch (e:Exception) {
                    e.printStackTrace();
                    //Toast.makeText(this, "ERROR DE URI CHIDÍSIMO: "+e.toString(), Toast.LENGTH_SHORT).show()
                }





                btnPlay2Adjuntado.visibility=View.VISIBLE
                borrar2.visibility=View.VISIBLE

            }



        }

    }
    private fun resetear() {
        chronometer.base = SystemClock.elapsedRealtime()
        timeWhenStopped = 0
    }

    private fun pararGrabar() {
        stopbtn!!.isEnabled = false
        stopbtn!!.setImageResource(R.drawable.stop_gris)
        startbtn!!.isEnabled = true
        startbtn!!.setImageResource(R.drawable.grabar)
        playbtn!!.isEnabled = true
        playbtn!!.setImageResource(R.drawable.play)

        mRecorder!!.stop()
        mRecorder!!.release()
        mRecorder = null
        //Toast.makeText(applicationContext, "Recording Stopped", Toast.LENGTH_LONG).show()
        esto!!.stopRippleAnimation()
        timeWhenStopped = chronometer.base - SystemClock.elapsedRealtime()
        chronometer.stop()

        adjuntarAudio.isEnabled=true
        mostrarGuia3()




    }

    private fun mostrarGuia3() {
        val prefManager6 = PreferenceManager.getDefaultSharedPreferences(this)
        if (!prefManager6.getBoolean("DMAKDSSA", false)){
            TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.borrar),
                "Elimine Audio", "Eliminar."
            )
                .cancelable(false)
                .drawShadow(true)
                .dimColor(R.color.Dorado)
                .outerCircleColor(R.color.colorAccent)
                .targetCircleColor(R.color.colorPrimaryDark)
                .tintTarget(false), object : TapTargetView.Listener() {
                override fun onTargetClick(view: TapTargetView) {
                    super.onTargetClick(view)
                    val prefEditor = prefManager6.edit()
                     prefEditor.putBoolean("DMAKDSSA", true)
                     prefEditor.apply()
                    //sequence.start()
                }

                override fun onOuterCircleClick(view: TapTargetView) {
                    super.onOuterCircleClick(view)
                    Toast.makeText(view.context, "tocaste fuera del circulo", Toast.LENGTH_SHORT).show()
                }

                override fun onTargetDismissed(view: TapTargetView, userInitiated: Boolean) {
                    Log.d("TapTargetViewSample", "You dismissed me :(")
                }

            })
        }else{
            //Toast.makeText(this, "Ya no muestro", Toast.LENGTH_LONG).show()
        }
    }

    private fun iniciarGrabar() {
        borrar.visibility=View.GONE
        if (btnPlay2.getVisibility()==View.VISIBLE){
            btnPlay2.visibility=View.GONE
            playbtn!!.visibility=View.VISIBLE
        }


        if (mPlayer != null) {
            mPlayer!!.reset();
            mPlayer!!.release();
            mPlayer= null;
            esto!!.stopRippleAnimation()
        }
        if(mPlayerito !=null){
            mPlayerito!!.reset()
            mPlayerito!!.release()
            mPlayerito=null
            esto!!.stopRippleAnimation()
        }
        if(mRecorder !=null){
            mRecorder!!.reset()
            mRecorder!!.release()
            mRecorder=null
        }else{
        }
        if (CheckPermissions()) {
           /* btnPlay2.visibility=View.GONE
            playbtn!!.visibility=View.VISIBLE*/
            stopbtn!!.isEnabled = true
            stopbtn!!.setImageResource(R.drawable.stop)
            startbtn!!.isEnabled = false
            startbtn!!.setImageResource(R.drawable.grabar_gris)
            playbtn!!.isEnabled = false
            playbtn!!.setImageResource(R.drawable.play_gris)
            adjuntarAudio.isEnabled=false
            //siguienteActividad3.isEnabled=false


            esto!!.startRippleAnimation()
            mRecorder = MediaRecorder()
            mRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
            mRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            mRecorder!!.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
            mRecorder!!.setMaxDuration(900000)
            mRecorder!!.setOutputFile(mFileName)
            try {
                mRecorder!!.prepare()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }

            mRecorder!!.start()
            mRecorder!!.setOnInfoListener({ mediaRecorder: MediaRecorder, i: Int, i1: Int ->
                if (i == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                    Toast.makeText(baseContext, "Tiempo permitido terminado", Toast.LENGTH_SHORT).show()
                    mRecorder!!.stop()
                    esto!!.stopRippleAnimation()
                    timeWhenStopped = chronometer.base - SystemClock.elapsedRealtime()
                    chronometer.stop()
                    stopbtn!!.isEnabled = false
                    startbtn!!.isEnabled = false
                    playbtn!!.isEnabled = true

                }





            })

        } else {
            RequestPermissions()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_AUDIO_PERMISSION_CODE -> if (grantResults.size > 0) {
                val permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val permissionToStore = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (permissionToRecord && permissionToStore) {
                   // Toast.makeText(applicationContext, "Permission Granted", Toast.LENGTH_LONG).show()
                } else {
                   // Toast.makeText(applicationContext, "Permission Denied", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun CheckPermissions(): Boolean {
        val result = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result1 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.RECORD_AUDIO)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED

    }

    private fun RequestPermissions() {
        ActivityCompat.requestPermissions(this@AudioActivity, arrayOf(
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ), REQUEST_AUDIO_PERMISSION_CODE)
    }


    companion object {
        private val LOG_TAG = "AudioRecording"
        private var mFileName: String? = null
        var audioFileUri: String?=null
        val REQUEST_AUDIO_PERMISSION_CODE = 1
    }
}