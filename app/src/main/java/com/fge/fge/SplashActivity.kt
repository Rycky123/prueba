package com.fge.fge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_session.*

class SplashActivity : AppCompatActivity() {
    var mAuth: FirebaseAuth? = null


    private fun toast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mAuth = FirebaseAuth.getInstance()

        val background = object : Thread() {
            override fun run() {
                try {
                    Thread.sleep(2000)
                    if (mAuth?.currentUser == null) {
                        val i = Intent()
                        i.setClass(baseContext, SessionActivity::class.java!!)
                        startActivity(i)
                        overridePendingTransition(
                            R.anim.fui_slide_in_right,
                            R.anim.fui_slide_out_left
                        )
                        finish()
                    } else {
                        val i = Intent()
                        i.setClass(baseContext, InicioActivity::class.java!!)
                        startActivity(i)
                        overridePendingTransition(
                            R.anim.fui_slide_in_right,
                            R.anim.fui_slide_out_left
                        )
                        finish()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }
        background.start()


    }

}
