package com.fge.fge

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.MotionEvent
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.fge.fge.Models.GlobalClass
import kotlinx.android.synthetic.main.activity_formulario_predenuncia.*
import org.json.JSONObject
import java.lang.Exception
import com.google.android.gms.location.LocationSettingsStatusCodes
import android.content.IntentSender
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.FragmentActivity
import com.android.volley.RequestQueue
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.getkeepsafe.taptargetview.TapTargetView
import com.google.android.gms.location.LocationSettingsResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.toptoche.searchablespinnerlibrary.SearchableSpinner
import kotlinx.android.synthetic.main.cancelar_predenuncia.view.*
import kotlinx.android.synthetic.main.encender_ubicacion.view.*


class FormularioPredenunciaActivity : AppCompatActivity(),AdapterView.OnItemSelectedListener {

    override fun onBackPressed() {

        var int=0
        int++
        if (int==1){
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.cancelar_predenuncia, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val  mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.seguirAlert.setOnClickListener {

                mAlertDialog.dismiss()
            }
            mDialogView.siAlert.setOnClickListener {
                val globalVariable = applicationContext as GlobalClass
                globalVariable.spinner=null
                globalVariable.narrativa=null
                globalVariable.ubicacion=null
                globalVariable.numero=null
                globalVariable.calle=null
                globalVariable.colonia=null
                globalVariable.ciudad=null
                globalVariable.estado=null
                globalVariable.pais=null
                globalVariable.cp=null
                globalVariable.audio=null
                globalVariable.imagenes=null
                globalVariable.contador=null
                globalVariable.videoSlected=null
                globalVariable.latitud=null
                globalVariable.longitud=null
                val i = Intent(baseContext, InicioActivity::class.java)
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(i)
                finish()

            }

        }else{
            super.onBackPressed()
        }
    }

    var languages = arrayOf("Seleccione tipo de Predenuncia","Lesiones dolosas", "Robo de vehículo con violencia", "Robo de vehículo sin violencia", "Robo de motocicleta con violencia",
        "Robo de motocicleta sin violencia",
        "Amenazas", "Daño en las cosas", "Daño en las cosas por hechos de tránsito", "Lesiones en razón de parentezco")
    var spinner: SearchableSpinner? = null
    var textView_msg: TextView? = null
    var Ubicacion:Button?=null

    //ubicacion
    private var locationManager : LocationManager? = null
    private var hasGSP=false
    private var hasNetwork=false
    private  var locationGPS : Location? =null
    private  var locationNetwork : Location?=null

    lateinit var numeroD:EditText
    lateinit var calle:EditText
    lateinit var colonia:EditText
    var munucipio:String=""
    var estado:String=""
    var pais:String=""
    lateinit var cp:EditText
    var direccion2:String=""
    lateinit var miniProgress:ProgressBar

    lateinit var narrativaFormulario:EditText

    lateinit var cancelar:Button
    lateinit var siguienteActividad2:ImageView

    internal lateinit var opcionesMunicipio: Array<String>


    private lateinit var progressBar: ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_formulario_predenuncia)

        mostrarGuia()

        scrollView.setVerticalScrollBarEnabled(false);
        scrollView.setHorizontalScrollBarEnabled(false);

        spinner = this.spinner_sample
        spinner!!.setOnItemSelectedListener(this)

        spinner!!.setTitle("Elije Tipo de predenuncia")
        spinner!!.setPositiveButton("Cerrar")



        progressBar= ProgressBar(this)
        progressBar=findViewById(R.id.progressBarFormuario)

        val aa = ArrayAdapter(this, R.layout.spinner_itemnuno, languages)
        aa.setDropDownViewResource(R.layout.spinner_itemnuno)
        spinner!!.setAdapter(aa)

        Ubicacion=findViewById(R.id.ubicacion)

        Ubicacion!!.setOnClickListener({
            getUbicacion()

        })

        miniProgress= ProgressBar(this)
        miniProgress=findViewById(R.id.miniP)

        cancelar=findViewById(R.id.cancelar)


        numeroD=findViewById(R.id.textNumeroD)
        calle=findViewById(R.id.textCalleD)
        colonia=findViewById(R.id.textColoniaD)
       // ciudadD=findViewById(R.id.textCiudadD)
        //estadoD=findViewById(R.id.textEstadoD)
        //paisD=findViewById(R.id.textPaisD)
        cp=findViewById(R.id.textCodigoP)

        narrativaFormulario=findViewById(R.id.narrativaFormulario)

        cancelar.setOnClickListener({

            val i = Intent(baseContext, InicioActivity::class.java)
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(i)
            finish()
        })

        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?;
        siguienteActividad2=findViewById(R.id.siguienteActividad2)
        val globalVariable = applicationContext as GlobalClass

        cancelar.setOnClickListener({
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.cancelar_predenuncia, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val  mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.seguirAlert.setOnClickListener {

                mAlertDialog.dismiss()
            }
            mDialogView.siAlert.setOnClickListener {
                val globalVariable = applicationContext as GlobalClass
                globalVariable.spinner=null
                globalVariable.narrativa=null
                globalVariable.ubicacion=null
                globalVariable.numero=null
                globalVariable.calle=null
                globalVariable.colonia=null
                globalVariable.ciudad=null
                globalVariable.estado=null
                globalVariable.pais=null
                globalVariable.cp=null
                globalVariable.audio=null
                globalVariable.imagenes=null
                globalVariable.contador=null
                globalVariable.videoSlected=null
                globalVariable.latitud=null
                globalVariable.longitud=null
                val i = Intent(baseContext, InicioActivity::class.java)
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(i)
                finish()

            }

        })

        val spinner = findViewById<View>(R.id.textCiudadD) as Spinner
        val adapter = ArrayAdapter.createFromResource(this, R.array.Municipio, R.layout.spinner_itemn)
        adapter.setDropDownViewResource(R.layout.spinner_itemn)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this

        opcionesMunicipio=this@FormularioPredenunciaActivity.resources.getStringArray(R.array.Municipio)
        siguienteActividad2.setOnClickListener({

                //Subimos datos y cambiamos de actividad
                val narrativa=narrativaFormulario.text.trim().toString()
                var failFlag:Boolean = false

                if( narrativa.isEmpty() )
                {
                    failFlag = true;
                    narrativaFormulario.setError( "Campo requerido" )
                }
                if(narrativa.length<15){
                    failFlag = true;
                    narrativaFormulario.setError( "Narrativa muy pequeña" );
                }
                if( textNumeroD.text.toString().trim().isEmpty())
                {
                    failFlag = true;
                    textNumeroD.setError( "Campo requerido" );
                }
                if( textCalleD.text.trim().toString().trim().isEmpty())
                {
                    failFlag = true;
                    textCalleD.setError( "Campo requerido" );
                }
                if(textColoniaD.text.trim().toString().trim().isEmpty() )
                {
                    failFlag = true;
                    textColoniaD.setError( "Campo requerido" );
                }
                if(textCodigoP.text.trim().toString().trim().isEmpty())
                {
                    failFlag = true;
                    textCodigoP.setError( "Campo requerido" );
                }
                if(textCodigoP.text.trim().length >5)
                {
                failFlag = true;
                textCodigoP.setError( "Código postal incorrecto" );
                 }
                if(textCodigoP.text.trim().length<5 )
                {
                    failFlag = true;
                    textCodigoP.setError( "Código postal incorrecto" );
                }
                if(textCodigoP.text.trim().contains("[A-Za]".toRegex()))
                {
                    failFlag = true;
                    textCodigoP.setError( "Código postal incorrecto" );
                }
                val spinner = findViewById<Spinner>(R.id.spinner_sample);
                val seleccionado = spinner.getSelectedItem().toString();
                val valor="Seleccione tipo de Predenuncia"
                if(seleccionado==valor){
                    failFlag = true;
                    errorSpinner.visibility= android.view.View.VISIBLE
                }else{
                    errorSpinner.visibility= android.view.View.GONE
                }

            val spinner2 = findViewById<Spinner>(R.id.textCiudadD);
            val seleccionad2 = spinner2.getSelectedItem().toString();
            val valor2="Municipio"
            if(seleccionad2==valor2){
                failFlag = true;
                errorSpinner2.visibility= android.view.View.VISIBLE
            }else{
                errorSpinner2.visibility= android.view.View.GONE
            }

                // if all are fine
                if (failFlag == false) {


                    globalVariable.spinner=seleccionado
                    globalVariable.narrativa=narrativa
                    globalVariable.numero=numeroD.text.toString()
                    globalVariable.calle=calle.text.toString()
                    globalVariable.colonia=colonia.text.toString()
                    globalVariable.ciudad=munucipio
                    globalVariable.estado=estado
                    globalVariable.pais=pais
                    globalVariable.cp=cp.text.toString()
                    globalVariable.ubicacion=direccion2.toString()
                    val i = Intent(baseContext, AudioActivity::class.java)
                    startActivity(i)
                    //overridePendingTransition(R.anim.slide_in_right, 0)


            }

        })

    }


    private fun mostrarGuia(){
        val prefManager2 = PreferenceManager.getDefaultSharedPreferences(this)
        if (!prefManager2.getBoolean("didididid", false)){


            val sequence = TapTargetSequence(this)
                .targets(
                    // This tap target will target the back button, we just need to pass its containing toolbar
                    TapTarget.forView(findViewById(R.id.spinner_sample), "Tipo de Denuncia",
                        "Seleccione.")
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.Dorado)
                        .id(1),
                    TapTarget.forView(findViewById(R.id.narrativaTitulo), "Narrativo",
                        "Narrar los hechos.")
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.colorAccent)
                        .id(2),
                    TapTarget.forView(findViewById(R.id.ubicacion), "Presione y active ubicación",
                        "O llene los campos manualmente.")
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.colorAccent)
                        .id(3),
                    TapTarget.forView(findViewById(R.id.siguienteActividad2), "Siguiente paso",
                        "Todos los campos son requeridos.")
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.Dorado)
                        .id(4)
                    )

            TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.cancelar),
                "Cancelar Pre-Denuncia", "Finalizar el proceso de Denuncia."
            )
                .cancelable(false)
                .drawShadow(true)
                .dimColor(R.color.Dorado)
                .outerCircleColor(R.color.Dorado)
                .targetCircleColor(R.color.colorPrimaryDark)
                .tintTarget(false), object : TapTargetView.Listener() {
                override fun onTargetClick(view: TapTargetView) {
                    super.onTargetClick(view)
                    val prefEditor = prefManager2.edit()
                     prefEditor.putBoolean("didididid", true)
                     prefEditor.apply()
                  sequence.start()
                }

                override fun onOuterCircleClick(view: TapTargetView) {
                    super.onOuterCircleClick(view)
                    Toast.makeText(view.context, "tocaste fuera del circulo", Toast.LENGTH_SHORT).show()
                }

                override fun onTargetDismissed(view: TapTargetView, userInitiated: Boolean) {
                    Log.d("TapTargetViewSample", "You dismissed me :(")
                }

            })
        }else{
            //Toast.makeText(this, "Ya no muestro", Toast.LENGTH_LONG).show()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getUbicacion() {
        locationManager=getSystemService(Context.LOCATION_SERVICE) as LocationManager
        hasGSP= locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        hasNetwork=locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

        if (hasGSP || hasNetwork){
            if (hasGSP){
                val header1=findViewById<android.view.View>(R.id.LENA)
                header1.visibility= android.view.View.VISIBLE
                miniProgress.visibility= android.view.View.VISIBLE

                locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER,500,0F, object :
                    LocationListener {
                    override fun onLocationChanged(location: Location?) {
                        if (location!=null){
                            locationGPS=location
                            //Toast.makeText(baseContext, "Latitud:"+location.latitude+"Longitud:"+location.longitude, Toast.LENGTH_SHORT).show()
                            //getVolley(location.latitude,location.longitude)
                        }
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                    }

                    override fun onProviderEnabled(provider: String?) {

                    }

                    override fun onProviderDisabled(provider: String?) {

                    }



                })

                val localGPSLocation=  locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                if (localGPSLocation!=null){
                    locationGPS=localGPSLocation
                }
            }
            if (hasNetwork){
                val header1=findViewById<View>(R.id.LENA)
                header1.visibility=View.VISIBLE
                miniProgress.visibility=View.VISIBLE

                locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,500,0F, object : LocationListener {
                    override fun onLocationChanged(location: Location?) {
                        if (location!=null){
                            locationNetwork=location
                        }
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                    }

                    override fun onProviderEnabled(provider: String?) {

                    }

                    override fun onProviderDisabled(provider: String?) {

                    }


                })

                val localNetworkLocation=  locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                if (localNetworkLocation!=null){
                    locationNetwork=localNetworkLocation
                }

                if (locationGPS!=null && locationNetwork!=null){
                    val globalVariable = applicationContext as GlobalClass

                    if (locationGPS!!.accuracy > locationNetwork!!.accuracy){
                        //Toast.makeText(baseContext,"LATITUD Y LONGITUD GPS:"+locationGPS!!.latitude+"," +locationGPS!!.longitude,Toast.LENGTH_SHORT).show()
                        getVolley(locationGPS!!.latitude, locationGPS!!.longitude)
                        globalVariable.latitud=locationGPS!!.latitude
                        globalVariable.longitud=locationGPS!!.longitude


                    }else{
                        getVolley(locationNetwork!!.latitude, locationNetwork!!.longitude)
                        //Toast.makeText(baseContext,"LATITUD Y LONGITUD GPS:"+locationNetwork!!.latitude+"," +locationNetwork!!.longitude,Toast.LENGTH_SHORT).show()
                        globalVariable.latitud=locationNetwork!!.latitude
                        globalVariable.longitud=locationNetwork!!.longitude
                    }
                }

            }


        }else{

            val mDialogView = LayoutInflater.from(this).inflate(R.layout.encender_ubicacion, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val  mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.noPrenderGPS.setOnClickListener {
                Toast.makeText(applicationContext,"Rellena los campos manualmente",Toast.LENGTH_SHORT).show()
                mAlertDialog.dismiss()
            }
            mDialogView.siPrenderGPS.setOnClickListener {
                mAlertDialog.dismiss()
                //startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                val googleApiClient = GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).build()
                googleApiClient.connect()

                val locationRequest = LocationRequest.create()
                locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

                val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
                builder.setAlwaysShow(true)

                val result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
                result.setResultCallback(object : ResultCallback<LocationSettingsResult> {
                    override fun onResult(result: LocationSettingsResult) {
                        val status = result.status
                        when (status.statusCode) {
                            LocationSettingsStatusCodes.SUCCESS ->
                                print("Encendido")
                            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                                try {
                                    // Muestro el diálogo porque está apagado
                                    // in onActivityResult(). el número es por si quieres agregar un onActivityResult
                                    //Yo sólo quiero el GPS prendido y listo, so no hago caso a eso
                                    status.startResolutionForResult(this@FormularioPredenunciaActivity, 101)
                                } catch (e: IntentSender.SendIntentException) {
                                }

                            }
                            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                                print("Error")

                        }
                    }
                })

            }

        }
    }


    private fun getVolley(latitude: Double, longitude: Double) {
        val longitud=longitude.toString()
        val latitud=latitude.toString()
        val requestQueue: RequestQueue


        try{
            val url="https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitud,$longitud&key=AIzaSyAK6-sGhbT7lFDE-jgu2WoAX-NuGOuF7TU"
            println("URL:"+url)
             requestQueue = Volley.newRequestQueue(this)
            val req= StringRequest(
                Request.Method.GET,url,
                Response.Listener {
                        response ->
                    println("ESTO: "+response.toString())
                    formatJSON(response.toString())
                }, Response.ErrorListener {
                    println("WROOOONGGGGGG")
                })
            requestQueue.run {
                add(req)
            }


        }catch (e: Exception) {
            e.printStackTrace()
            e.toString()
            println("ERROR DE VOLLEY: "+e)
        }
    }

    private fun formatJSON(jsonSTR: String) {
        miniProgress.visibility= android.view.View.GONE

        try {
            if(jsonSTR.isNotEmpty()) {
                val json = JSONObject(jsonSTR)
                val jsonResponse = json.getJSONArray("results")
                val jsonAvenida = jsonResponse.getJSONObject(0)
                direccion2 = jsonAvenida.getString("formatted_address")
                val direccion = jsonAvenida.getJSONArray("address_components")

                for (i in 0 until direccion.length()) {
                    val zero2 = direccion.getJSONObject(i)
                    val long_name = zero2.getString("long_name")
                    val mtypes = zero2.getJSONArray("types")
                    val Type = mtypes.getString(0)
                    if (Type.equals("street_number", ignoreCase = true)) {
                        numeroD.setText( long_name)
                    } else if (Type.equals("route", ignoreCase = true)) {
                        calle.setText(long_name)
                    } else if (Type.equals("political", ignoreCase = true)) {
                         colonia.setText(long_name)
                       // Toast.makeText(this, ""+long_name, Toast.LENGTH_LONG).show()
                    }
                    /*else if (Type.equals("locality", ignoreCase = true)) {
                        // Address2 = Address2 + long_name + ", ";
                         = long_name*/
                    /*} else if (Type.equals("administrative_area_level_2", ignoreCase = true)) {
                        County = long_name
                    } else if (Type.equals("administrative_area_level_1", ignoreCase = true)) {
                        State = long_name
                    } else if (Type.equals("country", ignoreCase = true)) {
                        Country = long_name*/
                    else if (Type.equals("postal_code", ignoreCase = true)) {
                        cp.setText(long_name)
                    }
                }

            }

        }catch (e: Exception) {
            e.printStackTrace()
            e.toString()
            println("ERROR DE FORMAT: "+e)
            //Si no encontramos ningún resultado

        }
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {

    }
    override fun onItemSelected(p0: AdapterView<*>?, p1: android.view.View?, p2: Int, p3: Long) {
        munucipio=opcionesMunicipio[p2]
        val spinner = findViewById<SearchableSpinner>(R.id.spinner_sample);
        val seleccionado = spinner.getSelectedItem().toString();
        val valor="Seleccione tipo de Predenuncia"
        if(seleccionado!=valor){
            errorSpinner.visibility= android.view.View.GONE
        }

        val spinner2 = findViewById<SearchableSpinner>(R.id.textCiudadD);
        spinner2.setTitle("Elije Municipio")
        spinner2.setPositiveButton("Cerrar")
        val seleccionado2 = spinner2.getSelectedItem().toString();
        val valor2="Municipio"
        if(seleccionado2!=valor2){
            errorSpinner2.visibility= android.view.View.GONE
        }



    }


}
