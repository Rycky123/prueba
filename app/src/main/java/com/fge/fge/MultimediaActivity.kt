package com.fge.fge

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fge.fge.Models.GlobalClass
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.getkeepsafe.taptargetview.TapTargetView
import kotlinx.android.synthetic.main.activity_multimedia.*
import kotlinx.android.synthetic.main.cancelar_predenuncia.view.*
import kotlinx.android.synthetic.main.video_visor.view.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import kotlin.collections.ArrayList

class MultimediaActivity : AppCompatActivity() {
    val nonDupList = ArrayList<String>();
    val nonDupList2 = ArrayList<String>();
    var nuevoNuevo = ArrayList<String>();

    var newfile: File? = null

    var banderaVideoPop: Boolean = false

    override fun onStart() {
        super.onStart()

        val globalVariable = applicationContext as GlobalClass
        /*val lalala=globalVariable.imagenes*/
        val b = this.intent.extras
        if (b != null) {
            if (b.containsKey("HOLA2")) {
                val lalala = b.getStringArrayList("HOLA2") as java.util.ArrayList<String>

                if (lalala.isNotEmpty()) {
                    //Toast.makeText(this, "onStart"+lalala+"size:"+lalala!!.size, Toast.LENGTH_SHORT).show()
                    for (x in 0 until lalala.size) {
                        nuevoNuevo.add(lalala.get(x))
                        //  Toast.makeText(this, "nuevoNuevo"+nuevoNuevo+"size:"+nuevoNuevo!!.size, Toast.LENGTH_SHORT).show()
                    }


                    // val wordDulicate =  ArrayList<String>();

                    val dupIter = nuevoNuevo.iterator();
                    while (dupIter.hasNext()) {
                        val dupWord = dupIter.next();
                        if (nonDupList.contains(dupWord)) {
                            dupIter.remove();
                        } else {
                            nonDupList.add(dupWord);
                        }
                    }
                    //Toast.makeText(this, "nonDupList"+nonDupList+"size:"+nonDupList!!.size, Toast.LENGTH_SHORT).show()
                    nuevoNuevo = nonDupList
                    //Toast.makeText(this, "nuevoNuevo2"+nuevoNuevo+"size:"+nuevoNuevo!!.size, Toast.LENGTH_SHORT).show()

                    //Toast.makeText(this, "onStart"+"size:"+nonDupList!!.size, Toast.LENGTH_SHORT).show()

                    //Toast.makeText(this, "Hola ESTÁ LLENO"+lalala, Toast.LENGTH_SHORT).show()
                    // resultado=nonDupList//esto amigo

                    resultado2.addAll(nuevoNuevo)
                    val dupIter2 = resultado2.iterator();
                    while (dupIter2.hasNext()) {
                        val dupWord = dupIter2.next();
                        if (nonDupList2.contains(dupWord)) {
                            dupIter2.remove();
                        } else {
                            nonDupList2.add(dupWord);
                        }
                    }
                    resultado = nonDupList2
                    //Toast.makeText(this, "resultado"+resultado+"size:"+resultado!!.size, Toast.LENGTH_SHORT).show()
                    val layoutManager = GridLayoutManager(this, 2)
                    list!!.layoutManager = layoutManager
                    recyclerAdapter = adapter(resultado, this)
                    // list!!.addItemDecoration(list!!.context, layoutManager.orientation)
                    list!!.adapter = recyclerAdapter


                } else {

                    //Toast.makeText(this, "VACIO", Toast.LENGTH_SHORT).show()

                }


                val video1 = globalVariable.videoSlected
                if (video1 != null) {
                    //Toast.makeText(this, "YA HAY VIDEO SE PONEE DE COLOR", Toast.LENGTH_SHORT).show()
                } else {
                    //Toast.makeText(this, "Ño HAY VIDEO", Toast.LENGTH_SHORT).show()


                }

                //video//////////////////////
                val banderaVide = globalVariable.videoSlected
                if (banderaVide != null) {
                    banderaVideoPop = true
                    //CAMBIO COLOR DEL VIDEO
                }


            }
        }

    }


    private var list: RecyclerView? = null
    private var recyclerAdapter: adapter? = null
    var resultado = ArrayList<String>()
    var resultado2 = ArrayList<String>()
    lateinit var button2: ImageButton
    lateinit var button3: ImageButton
    var contador: Int = 0
    var imageUri: Uri? = null
    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multimedia)


        scrul.setVerticalScrollBarEnabled(false);
        scrul.setHorizontalScrollBarEnabled(false);

        button2 = findViewById(R.id.botonArray)
        button3 = findViewById(R.id.botonCamara)
        list = findViewById<View>(R.id.list) as RecyclerView


        mostrarGuia()

        volver.setOnClickListener({
            val globalVariable = applicationContext as GlobalClass
            val conti = globalVariable.contador
            if (conti != null) {
                if (conti >= 1) {
                    if (resultado.isEmpty()) {
                        // globalVariable.imagenes=null
                        finish()
                        overridePendingTransition(
                            R.anim.fui_slide_in_right,
                            R.anim.fui_slide_out_left
                        )
                    } else {
                        recyclerAdapter!!.notifyDataSetChanged()
                        //Toast.makeText(this, "actuaizado:"+actuaizado!!.size, Toast.LENGTH_SHORT).show()
                        globalVariable.imagenes = resultado
                        // finish()
                        /*val intent = Intent(this, AudioActivity::class.java);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent);
                        overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)*/
                        val b = Bundle()
                        b.putStringArrayList("HOLA", resultado)
                        //Toast.makeText(baseContext, "galeri"+resultado, Toast.LENGTH_LONG).show()
                        val intent = Intent(this, AudioActivity::class.java)
                        intent.putExtras(b)
                        // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        overridePendingTransition(
                            R.anim.fui_slide_in_right,
                            R.anim.fui_slide_out_left
                        )
                    }
                }
            } else {
                contador++
                globalVariable.contador = contador
                if (resultado.isEmpty()) {
                    //finish()
                    finish()
                    overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
                } else {
                    recyclerAdapter!!.notifyDataSetChanged()
                    //Toast.makeText(this, "actuaizado:"+actuaizado!!.size, Toast.LENGTH_SHORT).show()
                    //globalVariable.imagenes=resultado
                    // finish()
                    /*val intent = Intent(this, AudioActivity::class.java);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent);
                    overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)*/
                    val b = Bundle()
                    b.putStringArrayList("HOLA", resultado)
                    //Toast.makeText(baseContext, "galeri"+resultado, Toast.LENGTH_LONG).show()
                    val intent = Intent(this, AudioActivity::class.java)
                    intent.putExtras(b)
                    // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
                }
            }
        })

        cancelar.setOnClickListener({
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.cancelar_predenuncia, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.seguirAlert.setOnClickListener {

                mAlertDialog.dismiss()
            }
            mDialogView.siAlert.setOnClickListener {
                val globalVariable = applicationContext as GlobalClass
                globalVariable.spinner = null
                globalVariable.narrativa = null
                globalVariable.ubicacion = null
                globalVariable.numero = null
                globalVariable.calle = null
                globalVariable.colonia = null
                globalVariable.ciudad = null
                globalVariable.estado = null
                globalVariable.pais = null
                globalVariable.cp = null
                globalVariable.audio = null
                globalVariable.audio2 = null
                globalVariable.imagenes = null
                globalVariable.contador = null
                globalVariable.videoSlected = null
                globalVariable.latitud = null
                globalVariable.longitud = null
                val i = Intent(baseContext, InicioActivity::class.java)
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(i)
                finish()

            }


        })

        siguienteActividad4.setOnClickListener({
            if (resultado.isEmpty()) {

                //Toast.makeText(this, "LO QUE SE VA A ENVIAR:"+resultado.size, Toast.LENGTH_SHORT).show()
                val intent = Intent(this, DetallesActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
                /* val globalVariable = applicationContext as GlobalClass
                 globalVariable.imagenes=resultado*/

            } else {
                recyclerAdapter!!.notifyDataSetChanged()
                /*val globalVariable = applicationContext as GlobalClass
                globalVariable.imagenes=null
                val i = Intent(baseContext, DetallesActivity::class.java)
                startActivity(i)*/
                val b = Bundle()
                b.putStringArrayList("HOLA3", resultado)
                //Toast.makeText(baseContext, "galeri"+resultado, Toast.LENGTH_LONG).show()
                val intent = Intent(this, DetallesActivity::class.java)
                intent.putExtras(b)
                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
                overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
            }

        })

        button2.setOnClickListener({
            val intent = Intent()

            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            //intent.action = Intent.ACTION_GET_CONTENT ASÍ ESTABA
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 779)
            button2.setBackgroundResource(R.drawable.galeria_normal)


        })
        button2.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> button2.setBackgroundResource(R.drawable.galeria_presion)

                }

                return v?.onTouchEvent(event) ?: true
            }
        })

        button3.setOnClickListener({
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                ) {
                    //NO HAY PERMISO
                    val arrayPermission = arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                    //muestra los permisos

                    requestPermissions(arrayPermission, 158799999)
                } else {
                    //Si hay permiso
                    openCamera()
                }
            } else {
                //menos que marshmallow
                openCamera()
            }

        })

        button3.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> button3.setBackgroundResource(R.drawable.camara_presion)

                }

                return v?.onTouchEvent(event) ?: true
            }
        })

        video.setOnClickListener({
            video.setBackgroundResource(R.drawable.video_normal)
            val popUpMenu = PopupMenu(this, it)
            popUpMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.cameraOption -> {
                        val intentRecord = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
                        startActivityForResult(intentRecord, 151)

                        true
                    }
                    R.id.galleryOption -> {
                        val intent = Intent()
                        intent.type = "video/*"
                        intent.action = Intent.ACTION_GET_CONTENT
                        startActivityForResult(Intent.createChooser(intent, "Select Video"), 1090)
                        true
                    }
                    R.id.borrarVideoMenu -> {
                        val globalVariable = applicationContext as GlobalClass
                        globalVariable.videoSlected = null
                        banderaVideoPop = false
                        //cambiamos a la vista normal del video
                        true
                    }
                    else -> false
                }

            }
            popUpMenu.inflate(R.menu.menu_video)
            try {
                val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
                fieldMPopup.isAccessible = true
                if (banderaVideoPop == true) {
                    popUpMenu.menu.findItem(R.id.borrarVideoMenu).isVisible = true
                } else if (banderaVideoPop == false) {
                    popUpMenu.menu.findItem(R.id.borrarVideoMenu).isVisible = false
                }
                val mPopup = fieldMPopup.get(popUpMenu)
                mPopup.javaClass
                    .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                    .invoke(mPopup, true)
            } catch (e: Exception) {
                Log.e("Formulario", "Error showing menu icons", e)
            } finally {
                popUpMenu.show()
            }


        })

        video.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> video.setBackgroundResource(R.drawable.video_presion)

                }

                return v?.onTouchEvent(event) ?: true
            }
        })

    }

    private fun mostrarGuia() {
        val prefManager3 = PreferenceManager.getDefaultSharedPreferences(this)
        if (!prefManager3.getBoolean("lalalaa", false)) {


            val sequence = TapTargetSequence(this)
                .targets(
                    // This tap target will target the back button, we just need to pass its containing toolbar
                    TapTarget.forView(
                        findViewById(R.id.botonArray), "Adjunte Imágenes",
                        "Adjunte imágenes desde su galería."
                    )
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.Dorado)
                        .id(1),
                    TapTarget.forView(
                        findViewById(R.id.video), "Adjunte Video",
                        "Adjunte video desde su cámara o galería."
                    )
                        .cancelable(false)
                        .drawShadow(true)
                        .targetCircleColor(R.color.colorPrimaryDark)
                        .tintTarget(false)
                        .transparentTarget(false)
                        .textColor(android.R.color.white)
                        .dimColor(R.color.Dorado)
                        .outerCircleColor(R.color.colorAccent)
                        .id(2)
                )

            TapTargetView.showFor(this, TapTarget.forView(
                findViewById(R.id.botonCamara),
                "Ajuntar Fotos", "Toma fotografías directamente desde la cámara de tu celular."
            )
                .cancelable(false)
                .drawShadow(true)
                .dimColor(R.color.Dorado)
                .outerCircleColor(R.color.colorAccent)
                .targetCircleColor(R.color.colorPrimaryDark)
                .tintTarget(false), object : TapTargetView.Listener() {
                override fun onTargetClick(view: TapTargetView) {
                    super.onTargetClick(view)
                    val prefEditor = prefManager3.edit()
                    prefEditor.putBoolean("lalalaa", true)
                    prefEditor.apply()
                    sequence.start()
                }

                override fun onOuterCircleClick(view: TapTargetView) {
                    super.onOuterCircleClick(view)
                    Toast.makeText(
                        view.context,
                        "tocaste fuera del circulo",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onTargetDismissed(view: TapTargetView, userInitiated: Boolean) {
                    Log.d("TapTargetViewSample", "You dismissed me :(")
                }

            })
        } else {
            //Toast.makeText(this, "Ya no muestro", Toast.LENGTH_LONG).show()
        }
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri =
            getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, 5892);
        button3.setBackgroundResource(R.drawable.camara_normal)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            158799999 -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permiso para el permiso fue concedido

                    openCamera()
                } else {
                    //Toast.makeText(this, "Permiso no concedido, no podrá hacer uso de la función", Toast.LENGTH_SHORT).show()
                }
                if (grantResults.size > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(this, "STORAGE", Toast.LENGTH_SHORT).show()
                } else {
                    //Toast.makeText(this, "Permiso no concedido, no podrá hacer uso de la función", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 779) {
            if (resultCode == RESULT_OK) {
                if (data == null) {
                    // something is wrong
                    //Toast.makeText(baseContext, "No seleccionó imagen", Toast.LENGTH_LONG).show()
                }
                val clipData = data?.clipData
                if (clipData != null) {// handle multiple photo
                    // Toast.makeText(this, "VARIAS FOTOS GALERÍA", Toast.LENGTH_SHORT).show()
                    for (i in 0 until clipData.itemCount) {
                        val uri = clipData.getItemAt(i).uri
                        resultado.add(uri.toString())
                        //Toast.makeText(baseContext, "galeri"+resultado.size, Toast.LENGTH_LONG).show()
                        val layoutManager = GridLayoutManager(this, 2)
                        list!!.layoutManager = layoutManager
                        recyclerAdapter = adapter(resultado, this)
                        // list!!.addItemDecoration(list!!.context, layoutManager.orientation)
                        list!!.adapter = recyclerAdapter

                    }

                } else { // una foto
                    val uri = data?.data
                    // Toast.makeText(this, "UNA FOTO GALERÍA"+uri.toString(), Toast.LENGTH_SHORT).show()
                    resultado.add(uri.toString())
                    val layoutManager = GridLayoutManager(this, 2)
                    list!!.layoutManager = layoutManager
                    recyclerAdapter = adapter(resultado, this)
                    // list!!.addItemDecoration(list!!.context, layoutManager.orientation)
                    list!!.adapter = recyclerAdapter

                }
            }
        }


        if (requestCode == 5892) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    /*val thumbnail = MediaStore.Images.Media.getBitmap(
                            getContentResolver(), imageUri);
                    imgView.setImageBitmap(thumbnail);*/
                    getRealPathFromURI(imageUri);


                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

        }
        ////////////////////////////////////////////////////////////////video////////////////////////////////////////////
        var videoUri: Uri? = null
        videoUri = data?.data
        if (requestCode == 151) {
            if (resultCode == Activity.RESULT_OK) {
                val globalVariable2 = applicationContext as GlobalClass

                //Toast.makeText(this, "Video guardado en "+videoUri, Toast.LENGTH_SHORT).show()
                val mDialogView = LayoutInflater.from(this).inflate(R.layout.video_visor, null)
                //AlertDialogBuilder
                val mBuilder = android.app.AlertDialog.Builder(this)
                    .setMessage(Html.fromHtml("Video seleccionado"))
                    .setView(mDialogView)

                mDialogView.videito.setVideoURI(videoUri)
                mDialogView.videito.setOnPreparedListener({
                    val videoDuration = mDialogView.videito.getDuration();
                    //Toast.makeText(this, "DURACIÓN:"+videoDuration, Toast.LENGTH_SHORT).show()
                    if (videoDuration > 300000) {
                        Toast.makeText(
                            this,
                            "Video demasiado grande, seleccione otro otro",
                            Toast.LENGTH_SHORT
                        ).show()
                        mDialogView.guardar.visibility = View.GONE
                    } else {
                        mDialogView.videito.start()
                        mDialogView.guardar.visibility = View.VISIBLE
                    }

                })

                mDialogView.videito.setOnCompletionListener({
                    mDialogView.videito.start()
                })

                //show dialog
                val mAlertDialog = mBuilder.show()
                //login button click of custom layout
                mDialogView.volver.setOnClickListener({
                    mAlertDialog.dismiss()
                })

                mDialogView.guardar.setOnClickListener({
                    try {

                        val videoAsset =
                            getContentResolver().openAssetFileDescriptor(data!!.getData()!!, "r");
                        val init = videoAsset!!.createInputStream();

                        val filepath = Environment.getExternalStorageDirectory();
                        val dir =
                            File(filepath.getAbsolutePath() + "/" + "Evidencia Fiscalia" + "/" + "/" + "Videos");
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }

                        newfile = File(dir, "VIDEO" +/*System.currentTimeMillis()+*/".mp4");

                        if (newfile!!.exists()) newfile!!.delete();

                        val out = FileOutputStream(newfile!!);

                        val buffer = ByteArray(1024)
                        var length = init.read(buffer)

                        //Transferring data
                        while (length != -1) {
                            out.write(buffer, 0, length)
                            length = init.read(buffer)
                        }

                        //Finalizing
                        out.flush()
                        out.close()
                        out.close()
                        //Toast.makeText(this, "éxito total", Toast.LENGTH_SHORT).show()

                        globalVariable2.videoSlected = newfile.toString()


                    } catch (e: Exception) {
                        e.printStackTrace();
                        //Toast.makeText(this, "ERROR DE URI CHIDÍSIMO: "+e.toString(), Toast.LENGTH_SHORT).show()
                    }
                    banderaVideoPop = true
                    mAlertDialog.dismiss()

                })

            }
        }

        if (requestCode == 1090) {
            if (resultCode == Activity.RESULT_OK) {

                val globalVariable2 = applicationContext as GlobalClass
                val selectedVideo = data!!.getData();
                //Toast.makeText(this, "Video guardado en "+videoUri, Toast.LENGTH_SHORT).show()
                val mDialogView = LayoutInflater.from(this).inflate(R.layout.video_visor, null)
                //AlertDialogBuilder
                val mBuilder = android.app.AlertDialog.Builder(this)
                    .setMessage(Html.fromHtml("Video seleccionado"))
                    .setView(mDialogView)

                mDialogView.videito.setVideoURI(selectedVideo)
                mDialogView.videito.setOnPreparedListener({
                    val videoDuration = mDialogView.videito.getDuration();
                    if (videoDuration > 300000) {
                        Toast.makeText(
                            this,
                            "Video demasiado grande, seleccione otro",
                            Toast.LENGTH_SHORT
                        ).show()
                        mDialogView.guardar.visibility = View.GONE
                    } else {

                        mDialogView.videito.start()
                        mDialogView.guardar.visibility = View.VISIBLE
                    }

                })

                mDialogView.videito.setOnCompletionListener({
                    mDialogView.videito.start()
                })

                //show dialog
                val mAlertDialog = mBuilder.show()
                //login button click of custom layout
                mDialogView.volver.setOnClickListener({
                    mAlertDialog.dismiss()
                })

                mDialogView.guardar.setOnClickListener({
                    try {


                        val videoAsset =
                            getContentResolver().openAssetFileDescriptor(data.getData()!!, "r");
                        val init = videoAsset!!.createInputStream();

                        val filepath = Environment.getExternalStorageDirectory();
                        val dir =
                            File(filepath.getAbsolutePath() + "/" + "Evidencia Fiscalia" + "/" + "/" + "videos");
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }

                        newfile = File(dir, "VIDEO" +/*System.currentTimeMillis()+*/".mp4");

                        if (newfile!!.exists()) newfile!!.delete();


                        val out = FileOutputStream(newfile!!);

                        val buffer = ByteArray(1024)
                        var length = init.read(buffer)

                        //Transferring data
                        while (length != -1) {
                            out.write(buffer, 0, length)
                            length = init.read(buffer)
                        }

                        //Finalizing
                        out.flush()
                        out.close()
                        out.close()
                        //Toast.makeText(this, "éxito total", Toast.LENGTH_SHORT).show()

                        globalVariable2.videoSlected = newfile.toString()


                    } catch (e: Exception) {
                        e.printStackTrace();
                        // Toast.makeText(this, "ERROR DE URI CHIDÍSIMO: "+e.toString(), Toast.LENGTH_SHORT).show()
                    }
                    banderaVideoPop = true
                    mAlertDialog.dismiss()
                })
            }

        }
        ////////////////////////////////////////////////////////////////video////////////////////////////////////////////

    }

    private fun getRealPathFromURI(imageUri: Uri?) {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = managedQuery(imageUri, proj, null, null, null)
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        /*return*/
        val esto = cursor.getString(column_index)
        val path = MediaStore.Images.Media.insertImage(contentResolver, esto, "Title", null)

        if (path != null) {
            //Toast.makeText(this, ""+esto, Toast.LENGTH_LONG).show()
            resultado.add(path)
            val layoutManager = GridLayoutManager(this, 2)
            list!!.layoutManager = layoutManager
            recyclerAdapter = adapter(resultado, this)
            // list!!.addItemDecoration(list!!.context, layoutManager.orientation)
            list!!.adapter = recyclerAdapter
        } else {
            // Toast.makeText(this, "null", Toast.LENGTH_LONG).show()
        }

    }

}

class adapter(val items: ArrayList<String>, val context: Context) :
    RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = items.get(position)
        holder.bind(item, context)
        holder.imagenTache.setOnClickListener({
            val itemLabel = items.get(position)
            items.removeAt(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, items.size)
            //Toast.makeText(context, "ADAPTER:"+items.size, Toast.LENGTH_SHORT).show()

        })

    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val imagenalv = view.findViewById(R.id.imagencita) as ImageView
    val imagenTache = view.findViewById(R.id.tache) as ImageView

    fun bind(objetito: String, context: Context) {
        itemView.setOnClickListener(View.OnClickListener {
            // Toast.makeText(context, objetito.toString(), Toast.LENGTH_SHORT).show()

        })
        imagenalv.loadUrl(objetito)
    }

    fun ImageView.loadUrl(url: String) {
        Glide.with(context)
            .load(url)
            .into(this)
    }


}


