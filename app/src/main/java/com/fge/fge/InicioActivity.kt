package com.fge.fge

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_inicio.*
import kotlinx.android.synthetic.main.confirmacion_sesion.view.*
import kotlinx.android.synthetic.main.permisos_app.view.*

class InicioActivity : AppCompatActivity() {

    lateinit var mAuth: FirebaseAuth
    private lateinit var userBD: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var dbReference: DatabaseReference
    private lateinit var imagenFoto:ImageView
    private lateinit var perfilButton:LinearLayout
    private lateinit var sesionButton:LinearLayout
    private lateinit var vehiculosRecuperados:LinearLayout
    private lateinit var hasVistoA:LinearLayout
    private lateinit var acercaDe:LinearLayout
    private lateinit var alertaAmber:LinearLayout

  private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener{ item->

        when(item.itemId){
            R.id.home->{
                replaceFragment(HomeFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.denuncias->{
                replaceFragment(DenunciasFragment())
                println("Denuncias presionado")
                return@OnNavigationItemSelectedListener true
            }
            R.id.ubica->{
                replaceFragment(FiscaliasFragment())
                println("Denuncias presionado")
                return@OnNavigationItemSelectedListener true
            }
        }
        false

    }
    private fun replaceFragment(Fragment: Fragment){
        val fragmentTransaction= supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer,Fragment)
        fragmentTransaction.commit()


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inicio)
        imagenFoto=findViewById(R.id.imageCard)


        val drawerLayout = findViewById<DrawerLayout>(R.id.drawerLayout)
        val content = findViewById<LinearLayout>(R.id.content)

       val toolbar = findViewById<Toolbar>(R.id.toolbar);
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.lineas_menu);


        val actionBarDrawerToggle = object : ActionBarDrawerToggle(this, drawerLayout,toolbar, R.string.open, R.string.close) {
            private val scaleFactor = 4f
            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                toolbar.setNavigationIcon(R.drawable.lineas_menu);
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                if (drawerView != null) {
                    super.onDrawerSlide(drawerView, slideOffset)
                    toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
                }
                val slideX = drawerView.width * slideOffset
                content.translationX = slideX
                content.scaleX = 1 - slideOffset / scaleFactor
                content.scaleY = 1 - slideOffset / scaleFactor
            }
        }

        drawerLayout.setScrimColor(Color.TRANSPARENT)
        drawerLayout.drawerElevation = 4f
        drawerLayout.addDrawerListener(actionBarDrawerToggle)


        replaceFragment(HomeFragment())
        if(!CheckPermissionFromDevice())
            requestPermission()

       bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        mAuth = FirebaseAuth.getInstance()

        if (mAuth.currentUser == null) {


        }else {
            val uid = FirebaseAuth.getInstance().currentUser!!.uid
            database = FirebaseDatabase.getInstance()
            dbReference = database.reference.child("Usuarios/")
            userBD = dbReference.child(uid)


            userBD.child("ImagenPerfil").addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()){
                        val CURP:String= snapshot.value.toString()
                        //Picasso.get().load(CURP).into(imagenFoto)
                        Picasso.get()
                            .load(CURP)
                            .into(imagenFoto)

                    }else{
                        imagenFoto.setImageResource(R.drawable.perfil_foto)
                    }



                }

                override fun onCancelled(error: DatabaseError) {}
            })
            userBD.child("InformacionPersonal").child("Nombre").addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()){
                        val CURP:String= snapshot.value.toString()
                        Nombre.setText(CURP)

                    }else{
                        Nombre.visibility=View.GONE
                    }



                }

                override fun onCancelled(error: DatabaseError) {}
            })

            perfilButton=findViewById(R.id.perfilButton)
            perfilButton.setOnClickListener({
                val intento1 = Intent(this, PerfilActivity::class.java)
                startActivity(intento1)
            })
            sesionButton=findViewById(R.id.sesionPerfil)
            sesionButton.setOnClickListener({
                val mDialogView = LayoutInflater.from(this).inflate(R.layout.confirmacion_sesion, null)
                val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
                val  mAlertDialog = mBuilder.show()
                mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                mDialogView.noCerrarSesionConfirmacion.setOnClickListener {

                    mAlertDialog.dismiss()

                }
                mDialogView.cerrarSesionConfirmacion.setOnClickListener {
                    mAuth.signOut()
                    val i1 = Intent(this, SessionActivity::class.java)
                    startActivity(i1)
                    finish()

                }
            })
            vehiculosRecuperados=findViewById(R.id.vehiculosrEuperados)
            vehiculosRecuperados.setOnClickListener({
                val intento1 = Intent(this, AutosActivity::class.java)
                startActivity(intento1)
            })

            hasVistoA=findViewById(R.id.hasVisto)
            hasVistoA.setOnClickListener({
                val intento1 = Intent(this, HasVistoAActivity::class.java)
                startActivity(intento1)
            })
            acercaDe=findViewById(R.id.acercaDe)
            acercaDe.setOnClickListener({
                val intento1 = Intent(this, AcercaDeActivity::class.java)
                startActivity(intento1)
            })

            alertaAmber=findViewById(R.id.alertaAmber)
            alertaAmber.setOnClickListener({
               OpenFacebookPage()
            })
        }
    }

    private fun OpenFacebookPage() {
        // FacebookページのID
        val facebookPageID = "AlertaAmberMich";

        // URL
        val facebookUrl = "https://www.facebook.com/" + facebookPageID;

        // URLスキーム
        val facebookUrlScheme = "fb://page/" + facebookPageID;

        try {
            // Facebookアプリのバージョンを取得
            val versionCode = getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;

            if (versionCode >= 3002850) {
                // Facebook アプリのバージョン 11.0.0.11.23 (3002850) 以上の場合
                val uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                startActivity( Intent(Intent.ACTION_VIEW, uri));
            } else {
                // Facebook アプリが古い場合
                startActivity( Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrlScheme)));
            }
        } catch ( e:PackageManager.NameNotFoundException) {
            // Facebookアプリがインストールされていない場合は、ブラウザで開く
            startActivity( Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));

        }
    }


    private fun requestPermission() {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE) ?: return
        val str_name = sharedPref.getString("name", "")
        if (str_name!!.isEmpty()){
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.permisos_app, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val  mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.aceptarTerminos.setOnClickListener {
                ActivityCompat.requestPermissions(this as Activity,
                    arrayOf(
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.CALL_PRIVILEGED,
                        Manifest.permission.READ_EXTERNAL_STORAGE

                    ),102)
                val sharedPref2 = this.getPreferences(Context.MODE_PRIVATE) ?: return@setOnClickListener
                with(sharedPref2.edit()) {
                    putString("name", "ALEJANDRO")

                    commit()
                }
                mAlertDialog.dismiss()
            }

        }else{

        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            102 -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    //Toast.makeText(this, "XOXO PERO ÑO", Toast.LENGTH_LONG).show()


                } else {
                    //Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT).show()

                }
            }
        }
    }

    private fun CheckPermissionFromDevice(): Boolean {
        val phone= ContextCompat.checkSelfPermission(this as Activity, Manifest.permission.READ_PHONE_STATE)
        val audio= ContextCompat.checkSelfPermission(this as Activity, Manifest.permission.RECORD_AUDIO)
        val write= ContextCompat.checkSelfPermission(this as Activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val camera= ContextCompat.checkSelfPermission(this as Activity, Manifest.permission.CAMERA)
        val coarse= ContextCompat.checkSelfPermission(this as Activity, Manifest.permission.ACCESS_COARSE_LOCATION)
        val fine= ContextCompat.checkSelfPermission(this as Activity, Manifest.permission.ACCESS_FINE_LOCATION)
        val call= ContextCompat.checkSelfPermission(this as Activity, Manifest.permission.CALL_PHONE)
        val pcall= ContextCompat.checkSelfPermission(this as Activity, Manifest.permission.CALL_PRIVILEGED)
        val READ_STRORAGE= ContextCompat.checkSelfPermission(this as Activity, Manifest.permission.READ_EXTERNAL_STORAGE)



        return phone== PackageManager.PERMISSION_GRANTED && audio== PackageManager.PERMISSION_GRANTED && write== PackageManager.PERMISSION_GRANTED &&
                camera== PackageManager.PERMISSION_GRANTED && coarse== PackageManager.PERMISSION_GRANTED && fine== PackageManager.PERMISSION_GRANTED &&
                call== PackageManager.PERMISSION_GRANTED && pcall== PackageManager.PERMISSION_GRANTED && READ_STRORAGE== PackageManager.PERMISSION_GRANTED

    }

    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Vuelva a presionar para salír", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }
}
