package com.fge.fge


import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Environment
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.net.toUri
import androidx.viewpager.widget.ViewPager
import com.fge.fge.Adapters.ViewPagerAdapter
import com.fge.fge.Models.GlobalClass
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.getkeepsafe.taptargetview.TapTargetView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageMetadata
import com.synnapps.carouselview.CarouselView
import kotlinx.android.synthetic.main.activity_detalles.*
import kotlinx.android.synthetic.main.activity_detalles.audioButton
import kotlinx.android.synthetic.main.activity_detalles.fecha
import kotlinx.android.synthetic.main.activity_detalles.tpo_denuncia
import kotlinx.android.synthetic.main.cancelar_predenuncia.view.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DetallesActivity : AppCompatActivity() {

    private var mPlayerGuardado: MediaPlayer? = null
    var lalala = ArrayList<String>()
    lateinit var volver: ImageButton
    private lateinit var progressBar: ProgressBar
    lateinit var ubicacionSeleccionada: String
    internal lateinit var viewDialog: ViewDialog
    private lateinit var userBD: DatabaseReference
    private lateinit var database: FirebaseDatabase//para usar la bd

    private lateinit var dbReference: DatabaseReference//firebase
    var long: Long = 0

    var fileOutput: String? = null
    var fileOutput2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles)
        volver = findViewById(R.id.volver)

        mostrarGuia()

        viewDialog = ViewDialog(this)

        progressBar = ProgressBar(this)
        progressBar = findViewById(R.id.progressBarUser)

        volver.setOnClickListener({
            if (lalala.isNotEmpty()) {
                if (mPlayerGuardado != null) {
                    mPlayerGuardado!!.reset()
                    mPlayerGuardado!!.release()
                    mPlayerGuardado = null
                }
                finish()
                overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
            } else {
                if (mPlayerGuardado != null) {
                    mPlayerGuardado!!.reset()
                    mPlayerGuardado!!.release()
                    mPlayerGuardado = null
                }
                finish()
                overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
            }

        })

        cancelar.setOnClickListener({
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.cancelar_predenuncia, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.seguirAlert.setOnClickListener {

                mAlertDialog.dismiss()
            }
            mDialogView.siAlert.setOnClickListener {
                val globalVariable = applicationContext as GlobalClass
                globalVariable.spinner = null
                globalVariable.narrativa = null
                globalVariable.ubicacion = null
                globalVariable.numero = null
                globalVariable.calle = null
                globalVariable.colonia = null
                globalVariable.ciudad = null
                globalVariable.estado = null
                globalVariable.pais = null
                globalVariable.cp = null
                globalVariable.audio = null
                globalVariable.audio2 = null
                globalVariable.contador = null
                globalVariable.videoSlected = null
                globalVariable.latitud = null
                globalVariable.longitud = null
                val i = Intent(baseContext, InicioActivity::class.java)
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(i)
                finish()
            }
        })

        scrollView2.setVerticalScrollBarEnabled(false);
        scrollView2.setHorizontalScrollBarEnabled(false);

        val globalVariable = applicationContext as GlobalClass
        //Spinner///////////////////////////////////////////////////////////////////////////////////////////////////////
        tpo_denuncia.text = globalVariable.spinner.toString()
        //Narrativa/////////////////////////////////////////////////////////////////////////////////////////////////////
        //narrativaEscrita.text=globalVariable.narrativa
        narrativaEscrita.text = globalVariable.narrativa

        //Ubicación/////////////////////////////////////////////////////////////////////////////////////////////////////
        val esto = globalVariable.ubicacion
        if (esto!!.isEmpty()) {
            val numeroRecuperado = globalVariable.numero
            val calleRecuperado = globalVariable.calle
            val coloniaRecuperado = globalVariable.colonia
            val cpRecuperado = globalVariable.cp
            val valor =
                "$calleRecuperado $numeroRecuperado, $coloniaRecuperado, $cpRecuperado. Morelia, Mich., Mexico"
            ubicacionDenuncia.text = valor

            ubicacionSeleccionada = valor
        } else {
            val numeroRecuperado = globalVariable.numero
            val calleRecuperado = globalVariable.calle
            val coloniaRecuperado = globalVariable.colonia
            val cpRecuperado = globalVariable.cp
            val valor =
                "$calleRecuperado $numeroRecuperado, $coloniaRecuperado, $cpRecuperado. Morelia, Mich., Mexico"
            ubicacionDenuncia.text = valor
            ubicacionSeleccionada = valor
        }


        //Imágenes/////////////////////////////////////////////////////////////////////////////////////////////////////
        val b = this.intent.extras
        if (b != null) {
            if (b.containsKey("HOLA3")) {
                lalala = b.getStringArrayList("HOLA3") as java.util.ArrayList<String>
                val viewPager = findViewById<ViewPager>(R.id.view_pager)
                titulo.visibility = View.VISIBLE
                titulo.text = "Evidencia fotográfica" + "(" + lalala!!.size.toString() + ")"
                viewPager.visibility = View.VISIBLE
                val adapter = ViewPagerAdapter(this, lalala!!)
                viewPager.adapter = adapter
            }
        } else {
            view_pager.visibility = View.GONE
            titulo.visibility = View.GONE
            Toast.makeText(baseContext, "ARRAY VACIO", Toast.LENGTH_SHORT).show()
        }

        //Video/////////////////////////////////////////////////////////////////////////////////////////////////////
        val videoSlected = globalVariable.videoSlected
        if (videoSlected != null) {
            videoTitulo.visibility = View.VISIBLE
            videoFinal.visibility = View.VISIBLE
            videoVistaBotones.visibility = View.VISIBLE
            //Toast.makeText(this, ""+videoSlected, Toast.LENGTH_SHORT).show()
            val uri = Uri.parse(videoSlected)
            videoFinal.setVideoURI(uri)
            videoFinal.start()
            videoReproducir.setOnClickListener({
                val uri = Uri.parse(videoSlected)
                videoFinal.setVideoURI(uri)
                videoFinal.start()
            })

            videoFinal.setOnCompletionListener {
                videoFinal.start()
            }

            videoParar.setOnClickListener({
                if (videoFinal.isPlaying) {
                    videoFinal.stopPlayback()
                }
            })

            val filepath = Environment.getExternalStorageDirectory();
            val fileDirectory =
                File(filepath.getAbsolutePath() + "/" + "Evidencia Fiscalia" + "/" + "/" + "Videos");
            val dirFiles = fileDirectory.listFiles();

            if (dirFiles.size != 0) {
                for (i in 0 until dirFiles.size) {
                    fileOutput2 = dirFiles[i].toString()
                    //Toast.makeText(this, "Hola, este es el video: "+fileOutput2, Toast.LENGTH_LONG).show()
                }
            }

        } else {
            videoFinal!!.visibility = View.GONE
            videoTitulo.visibility = View.GONE
        }
        //Fecha/////////////////////////////////////////////////////////////////////////////////////////////////////////
        val ubicacion = globalVariable.ubicacion
        val fechaDeHoy = Calendar.getInstance()
        val Fecha = SimpleDateFormat("MMM, d, y").format(fechaDeHoy.time)
        fecha.text = Fecha
        //Audio/////////////////////////////////////////////////////////////////////////////////////////////////////
        val audio = globalVariable.audio
        if (audio != null) {
            audioVista1.visibility = View.VISIBLE
            // audioButton.setAlpha(1.0f)
            //audioButtonPararResumen.setAlpha(1.0f)
            //Toast.makeText(this, "Audio 1", Toast.LENGTH_SHORT).show()
            audioButton.setOnClickListener({

                mPlayerGuardado = MediaPlayer()
                try {
                    mPlayerGuardado!!.setDataSource(audio)
                    mPlayerGuardado!!.prepare()
                    mPlayerGuardado!!.start()
                    mPlayerGuardado!!.setOnCompletionListener {
                        audioButton!!.isEnabled = true
                    }

                    mPlayerGuardado!!.setOnCompletionListener({
                        audioButton!!.isEnabled = true
                        audioButtonPararResumen.isEnabled = false

                    })

                    if (mPlayerGuardado!!.isPlaying) {
                        audioButtonPararResumen.isEnabled = true
                    }

                } catch (e: IOException) {
                    Log.e("hola", "prepare() failed")
                }
            })

            audioButtonPararResumen.setOnClickListener({
                if (mPlayerGuardado != null) {
                    mPlayerGuardado!!.reset()
                    mPlayerGuardado!!.release()
                    mPlayerGuardado = null
                }
                audioButtonPararResumen.isEnabled = false
            })
        }

        //AUDIO 2//////////////////////////////////////////////////////////////////////////////////////////////////////
        val audio2 = globalVariable.audio2
        if (audio2 != null) {
            audioVista2.visibility = View.VISIBLE
            //Toast.makeText(this, "Audio 2", Toast.LENGTH_SHORT).show()
            audioButton2.setOnClickListener({

                mPlayerGuardado = MediaPlayer()
                try {
                    mPlayerGuardado!!.setDataSource(this, audio2.toUri())
                    mPlayerGuardado!!.prepare()
                    mPlayerGuardado!!.start()
                    mPlayerGuardado!!.setOnCompletionListener {
                        audioButton2!!.isEnabled = true
                    }

                    mPlayerGuardado!!.setOnCompletionListener({
                        audioButton2!!.isEnabled = true
                        audioButtonPararResumen2.isEnabled = false
                    })

                    if (mPlayerGuardado!!.isPlaying) {
                        audioButtonPararResumen2.isEnabled = true
                    }

                } catch (e: IOException) {
                    Log.e("hola", "prepare() failed")
                }
            })


            audioButtonPararResumen2.setOnClickListener({
                if (mPlayerGuardado != null) {
                    mPlayerGuardado!!.reset()
                    mPlayerGuardado!!.release()
                    mPlayerGuardado = null
                }
                audioButtonPararResumen2.isEnabled = false
            })

            val filepath = Environment.getExternalStorageDirectory();
            val fileDirectory =
                File(filepath.getAbsolutePath() + "/" + "Evidencia Fiscalia" + "/" + "/" + "Audios");
            val dirFiles = fileDirectory.listFiles();

            if (dirFiles.size != 0) {
                for (i in 0 until dirFiles.size) {
                    fileOutput = dirFiles[i].toString()
                    // Toast.makeText(this, "Hola, este es el video: "+fileOutput, Toast.LENGTH_LONG).show()
                }
            }
        } else {

        }
        //UBICACIÓN POR PARTES//////////////////////////////////////////////////////////////////////////////////////////

        val latitud = globalVariable.latitud
        val longitd = globalVariable.longitud


        val uid = FirebaseAuth.getInstance().currentUser!!.uid
        database = FirebaseDatabase.getInstance()
        dbReference = database.reference.child("Usuarios/")
        userBD = dbReference.child(uid)


        userBD.child("denuncias").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    long = snapshot.childrenCount

                }
            }

            override fun onCancelled(error: DatabaseError) {}
        })


        finalizarProceso.setOnClickListener({
            if (mPlayerGuardado != null) {
                mPlayerGuardado!!.reset()
                mPlayerGuardado!!.release()
                mPlayerGuardado = null
            }
            if (videoFinal.isPlaying) {
                videoFinal.stopPlayback()
            }

            val mDialogView =
                LayoutInflater.from(this).inflate(R.layout.confirmar_predenuncia, null)
            val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.seguirAlert.setOnClickListener {

                mAlertDialog.dismiss()
            }
            mDialogView.siAlert.setOnClickListener {
                mAlertDialog.dismiss()
                //progressBar.visibility=View.VISIBLE
                viewDialog.showDialog()

                var stringFolio = ""

                val numeroString = long.toString().length
                Log.d("numero", "$numeroString")
                val calendar = Calendar.getInstance()
                val month = (calendar.get(Calendar.MONTH) + 1)
                Toast.makeText(this, "$numeroString", Toast.LENGTH_SHORT).show()
                val longmas = long + 1
                if (numeroString == 1) {
                    stringFolio = ("PD" + "-" + "000" + "$longmas" + "-" + month + "19" + "M")
                }
                if (numeroString == 2) {
                    stringFolio = ("PD" + "-" + "00" + "$longmas" + "-" + month + "19" + "M")
                }
                if (numeroString == 3) {
                    stringFolio = ("PD" + "-" + "0" + "$longmas" + "-" + month + "19" + "M")
                }
                if (numeroString == 4) {
                    stringFolio = ("PD" + "-" + "$longmas" + "-" + month + "19" + "M")
                }

                val tipoDenuncia = globalVariable.spinner.toString()
                val narrativa = globalVariable.narrativa
                val hMap: HashMap<String, String> = HashMap()
                hMap.put("Id", stringFolio)
                hMap.put("TipoDenuncia", tipoDenuncia)
                hMap.put("Ubicacion", ubicacionSeleccionada)
                hMap.put("Fecha", Fecha)
                hMap.put("Narrativa", narrativa!!)
                hMap.put("Latitud", latitud.toString())
                hMap.put("Longitud", longitd.toString())
                userBD.child("denuncias").child(stringFolio).setValue(hMap).addOnCompleteListener({
                    //Toast.makeText(baseContext, "SUBIÓ PRIMORDIAL", Toast.LENGTH_SHORT).show()
                    globalVariable.spinner = null
                    globalVariable.narrativa = null
                    globalVariable.ubicacion = null
                    globalVariable.numero = null
                    globalVariable.calle = null
                    globalVariable.colonia = null
                    globalVariable.ciudad = null
                    globalVariable.estado = null
                    globalVariable.pais = null
                    globalVariable.cp = null
                    globalVariable.latitud = null
                    globalVariable.longitud = null
                })


                ///////////////fotos////////////////////////////////////////////////////////////////
                if (lalala != null) {
                    for (x in 0 until lalala!!.size) {
                        val filename = UUID.randomUUID().toString()
                        val ref2 =
                            FirebaseStorage.getInstance().getReference("/imagesDenuncia/$filename")

                        if (lalala.isNotEmpty()) {
                            ref2.putFile((lalala.get(x)).toUri())
                                .addOnSuccessListener {
                                    ref2.downloadUrl.addOnSuccessListener {
                                        userBD.child("denuncias").child(stringFolio)
                                            .child("Evidencia").child("Imagen" + (x + 1).toString())
                                            .setValue(it.toString())
                                    }
                                }
                                .addOnCanceledListener {

                                }
                                .addOnCompleteListener({
                                    //Toast.makeText(baseContext, "SUBIÓ FOTOS", Toast.LENGTH_SHORT).show()

                                    globalVariable.imagenes = null
                                    globalVariable.contador = null
                                    globalVariable.videoSlected = null


                                })
                        }
                    }
                }
                ///////////////audio////////////////////////////////////////////////////////////////
                if (audio != null) {
                    // Toast.makeText(baseContext, "AUDIO"+audio, Toast.LENGTH_SHORT).show()
                    val audioUri = Uri.fromFile(File(audio))
                    val audioName = UUID.randomUUID().toString()
                    val ref = FirebaseStorage.getInstance().getReference("/narrativaVoz/$audioName")
                    var metadata = StorageMetadata.Builder()
                        .setContentType("audio/mp3")
                        .build()
                    ref.putFile(audioUri!!, metadata)
                        .addOnSuccessListener {
                            //Toast.makeText(baseContext, "Guardo Audio", Toast.LENGTH_LONG).show()
                            ref.downloadUrl.addOnSuccessListener {
                                userBD.child("denuncias").child(stringFolio).child("NarrativaVoz")
                                    .setValue(it.toString())

                            }
                        }
                        .addOnCompleteListener({
                            // Toast.makeText(baseContext, "SUBIÓ AUDIO", Toast.LENGTH_SHORT).show()
                            globalVariable.audio = null
                        })
                        .addOnCanceledListener {
                            // Toast.makeText(this, "Error al subir audio", Toast.LENGTH_LONG).show()
                        }

                } else {
                    // Toast.makeText(baseContext, "AUDIO 1 VACIO", Toast.LENGTH_SHORT).show()
                }
                ///////////////audio2////////////////////////////////////////////////////////////////
                if (fileOutput != null) {
                    val audioUri = Uri.fromFile(File(fileOutput!!))
                    val audioName = UUID.randomUUID().toString()
                    val ref = FirebaseStorage.getInstance().getReference("/narrativaVoz/$audioName")
                    var metadata = StorageMetadata.Builder()
                        .setContentType("audio/mp3")
                        .build()
                    ref.putFile(audioUri!!, metadata)
                        .addOnSuccessListener {
                            //Toast.makeText(baseContext, "Guardo Audio", Toast.LENGTH_LONG).show()
                            ref.downloadUrl.addOnSuccessListener {
                                userBD.child("denuncias").child(stringFolio).child("NarrativaVoz")
                                    .setValue(it.toString())

                            }
                        }
                        .addOnCompleteListener({
                            //  Toast.makeText(baseContext, "SUBIÓ AUDIO2", Toast.LENGTH_SHORT).show()
                            globalVariable.audio2 = null
                            globalVariable.audio = null
                            //ELIMINAR RUTA, ARCHIVO DE AUDIO
                        })
                        .addOnCanceledListener {
                            //  Toast.makeText(baseContext, "ERROR AL SUBIR AUDIO2", Toast.LENGTH_SHORT).show()
                        }


                } else {
                    // Toast.makeText(baseContext, "AUDIO 2 VACIO", Toast.LENGTH_SHORT).show()
                }
                ///////////////video////////////////////////////////////////////////////////////////
                if (fileOutput2 != null) {
                    val audioUri = Uri.fromFile(File(fileOutput2!!))
                    val audioName = UUID.randomUUID().toString()
                    val ref = FirebaseStorage.getInstance().getReference("/videos/$audioName")
                    var metadata = StorageMetadata.Builder()
                        .setContentType("video/mp4")
                        .build()
                    ref.putFile(audioUri!!, metadata)
                        .addOnSuccessListener {
                            //Toast.makeText(baseContext, "Guardo Audio", Toast.LENGTH_LONG).show()
                            ref.downloadUrl.addOnSuccessListener {
                                userBD.child("denuncias").child(stringFolio).child("Video")
                                    .setValue(it.toString())

                            }
                        }
                        .addOnCompleteListener({
                            // Toast.makeText(baseContext, "SUBIÓ video", Toast.LENGTH_SHORT).show()
                            globalVariable.videoSlected = null
                            //ELIMINAR RUTA, ARCHIVO DE AUDIO
                        })
                        .addOnCanceledListener {
                            // Toast.makeText(baseContext, "ERROR AL SUBIR VIDEO", Toast.LENGTH_SHORT).show()
                        }


                } else {
                    // Toast.makeText(baseContext, "VIDEO VACIO", Toast.LENGTH_SHORT).show()
                }

                // var progressValue = 0
                val timer = object : CountDownTimer(10000, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        // progressValue+=10
                        // progressBar.setProgress(progressValue)
                        //if(progressValue>=100){


                        // }
                    }

                    override fun onFinish() {
                        // progressBar.setVisibility(View.INVISIBLE)
                        viewDialog.hideDialog()
                        val i = Intent(baseContext, InicioActivity::class.java)
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(i)
                        finish()


                    }
                }
                timer.start()

            }


        })

    }

    private fun mostrarGuia() {
        val prefManager8 = PreferenceManager.getDefaultSharedPreferences(this)
        if (!prefManager8.getBoolean("skdad", false)) {
            TapTargetView.showFor(this, TapTarget.forView(
                findViewById(R.id.finalizarProceso),
                "Fin de la Pre-Denuncia", "Termine el proceso guardando sus datos."
            )
                .cancelable(false)
                .drawShadow(true)
                .dimColor(R.color.Dorado)
                .outerCircleColor(R.color.colorAccent)
                .targetCircleColor(R.color.colorPrimaryDark)
                .tintTarget(false), object : TapTargetView.Listener() {
                override fun onTargetClick(view: TapTargetView) {
                    super.onTargetClick(view)
                    val prefEditor = prefManager8.edit()
                    prefEditor.putBoolean("skdad", true)
                    prefEditor.apply()
                    //sequence.start()
                }

                override fun onOuterCircleClick(view: TapTargetView) {
                    super.onOuterCircleClick(view)
                    Toast.makeText(
                        view.context,
                        "tocaste fuera del circulo",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onTargetDismissed(view: TapTargetView, userInitiated: Boolean) {
                    Log.d("TapTargetViewSample", "You dismissed me :(")
                }

            })
        }
    }
}

