package com.fge.fge
class Denuncia {

    //Modelo de la denuncia

    var Identificador:String?=null
    var Estatus:String?=null
    var Imagen:String?= null

    constructor(){

    }

    constructor(identificador: String?, estatus: String?, imagen: String?) {
        this.Identificador = identificador
        this.Estatus = estatus
        this.Imagen = imagen
    }
}