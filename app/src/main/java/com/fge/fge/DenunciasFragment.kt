package com.fge.fge


import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.Display
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fge.fge.Adapters.RecyclerViewAdapter
import com.fge.fge.Models.ModelFirebase
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_denuncias.*
import kotlinx.android.synthetic.main.list_layout_denuncia.view.*
import java.util.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DenunciasFragment : Fragment() {

    var mRecycler: RecyclerView?=null
    private lateinit var userBD: DatabaseReference
    private lateinit var database: FirebaseDatabase//para usar la bd
    var firebaseUser: FirebaseUser?=null
    var firebaseAuth: FirebaseAuth?=null
    private lateinit var dbReference: DatabaseReference//firebase



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_denuncias, container, false)

        val uid = FirebaseAuth.getInstance().currentUser!!.uid
        database = FirebaseDatabase.getInstance()
        dbReference = database.reference.child("Usuarios/")
        userBD = dbReference.child(uid).child("denuncias")

        mRecycler=view.findViewById(R.id.listViewFirebase)
        mRecycler!!.setHasFixedSize(true)
        mRecycler!!.setLayoutManager(LinearLayoutManager(context))

        loadRecycler()

        return view
    }

    private fun loadRecycler() {
        val option = FirebaseRecyclerOptions.Builder<ModelFirebase>()
            .setQuery(userBD, ModelFirebase::class.java)
            .setLifecycleOwner(this)
            .build()

        val FirebaseRecyclerAdapter=object: FirebaseRecyclerAdapter<ModelFirebase, ModelFirebaseViewHolder>(option){
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelFirebaseViewHolder {
                val itemView = LayoutInflater.from(context).inflate(R.layout.list_layout_denuncia,parent,false)
                return ModelFirebaseViewHolder(itemView)
            }

            override fun onBindViewHolder(holder: ModelFirebaseViewHolder, p1: Int, model: ModelFirebase) {
                val placeid = getRef(p1).key.toString()

                userBD.child(placeid).addValueEventListener(object: ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        Toast.makeText(context, "Error Occurred "+ p0.toException(), Toast.LENGTH_SHORT).show()

                    }

                    override fun onDataChange(p0: DataSnapshot) {

                        if (p0.exists()){
                            if(progressBarUser != null) {
                                progressBarUser.visibility = if(itemCount == 0) View.VISIBLE else View.GONE
                            }


                            holder.Fecha.setText(model.Fecha)
                            holder.Tipo.setText(model.TipoDenuncia)
                            holder.Id.setText(placeid)
                            //Picasso.get().load(model.Image).into(holder.img_vet)

                            holder.itemView.setOnClickListener({
                                //Toast.makeText(context, "Click"+p1, Toast.LENGTH_SHORT).show()
                                val intent = Intent(context, PredenunciaResumen::class.java)
                                intent.putExtra("dato", placeid)
                                startActivity(intent)

                            })
                        }else{
                            mensaje.visibility=View.VISIBLE
                        }

                    }
                })

            }
        }

        mRecycler!!.adapter=FirebaseRecyclerAdapter
    }

    class ModelFirebaseViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        internal var Fecha: TextView = itemView!!.findViewById<TextView>(R.id.userEstatus)
        internal var Tipo: TextView = itemView!!.findViewById<TextView>(R.id.userIdentificador)
        internal var Id: TextView = itemView!!.findViewById<TextView>(R.id.userId)



    }




}