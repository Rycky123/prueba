package com.fge.fge


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import androidx.appcompat.app.AppCompatActivity
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.fge.fge.Models.GlobalClass
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.boton_llamada.view.*
import kotlinx.android.synthetic.main.encender_ubicacion.view.*


class FiscaliasFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener{
    private val RECORD_REQUEST_CODE = 102
    val call_code_request = 100

    private lateinit var mMap: GoogleMap
    lateinit var mMapView: MapView
    var mview: View? = null
    private lateinit var lastLocation: Location
    private var fusedLocationClient: FusedLocationProviderClient? =null

    var latitudMine:Double?=null
    var longitudMine:Double?=null


    private var locationManager : LocationManager? = null
    private var hasGSP=false
    private var hasNetwork=false



    private val Lazaro = LatLng(17.9604301,-102.19741)
    private val Arteaga= LatLng(18.3568607,-102.29263)
    private val Coahuyana= LatLng(18.6997773,-103.6613)
    private val Zamora= LatLng(20.0519607,-102.3263738)
    private val Reyes= LatLng(19.5869733,-102.46954)
    private val Zacapu= LatLng(19.8151022,-101.7869)
    private val Purepero= LatLng(19.8976994,-101.99819)
    private val Piedad= LatLng(20.3354132,-102.02035)
    private val Jiquilpan= LatLng(20.3354132,-102.02035)
    private val Puruandiro= LatLng(20.0826866,-101.51554)
    private val Sahuayo= LatLng(20.0926866,-101.51554)
    private val Tanhuato= LatLng(20.2859444,-102.32831)
    private val UruaanSur= LatLng(19.4000853,-102.0684774)
    private val UruaanNor= LatLng(19.4212025,-102.0541)
    private val Paracho= LatLng(19.4000853,-102.0684774)
    private val Taretan= LatLng(19.3303692,	-101.92079)
    private val Tacambaro= LatLng(19.2321718,-101.46328)
    private val Rosales= LatLng(19.2060472,	-101.71075)
    private val GZamora= LatLng(19.1577099,-102.057)
    private val Huacana= LatLng(18.9617504,-101.80851)
    private val snJuan= LatLng(19.4162726,-102.13189)
    private val Nocupetaro= LatLng(19.045706,-101.163616)
    private val Tancitaro= LatLng(19.336969,-102.36339)
    private val Hdalgo= LatLng(19.045706,-101.163616)
    private val Maravatio= LatLng(19.695000,-100.580056)
    private val Huetamo= LatLng( 18.628123,-100.899939)
    private val Tuxpan= LatLng(19.572013,-100.467261)
    private val Morelia= LatLng(19.682381, -101.234842)
    private val Zitacuaro= LatLng(19.420241,-100.330785)
   // private val Contepec= LatLng(19.9500606,-100.16794)
    private val Tlalpujahua= LatLng(19.803272,-100.172142)
    private val Apatzingan= LatLng(19.0870091,-102.35098)
    private val Aguililla= LatLng(18.7359925,-102.7928)
    private val Buenavista= LatLng(19.2106416,-102.59079)
    private val Tepalcatepec= LatLng(19.1892541,-102.84554)
   // private val Italia= LatLng(19.0141947,-102.09915)
    private val Coalcoman= LatLng(18.7785571,-103.16187)
    private val Estado= LatLng(19.681413,-101.234271)
    private val Violencia= LatLng(19.683514,-101.234656)
    private val AltoImpacto= LatLng(19.683802,-101.234656)
    private val Patzcuaro= LatLng(19.5107859,-101.6169495)
    private val EstadoMi= LatLng(19.674493,-101.220608)
    private val Zinapecuaro= LatLng(19.8536089,-100.8272261)
    var currentLatLng:LatLng?=null


    private var mLazaro: Marker? = null
    private var mArteaga: Marker? = null
    private var mCoahuyana: Marker? = null
    private var mZamora: Marker? = null
    private var mReyes: Marker? = null
    private var mZacapu: Marker? = null
    private var mPurepero: Marker? = null
    private var mPiedad: Marker? = null
    private var mJiquilpan: Marker? = null
    private var mPuruandiro: Marker? = null
    private var mSahuayo: Marker? = null
    private var mTanhuato: Marker? = null
    private var mUruapanSur: Marker? = null
    private var mUruapanNor: Marker? = null
    private var mParacho: Marker? = null
    private var mTaretan: Marker? = null
    private var mTacambaro: Marker? = null
    private var mRosales: Marker? = null
    private var mGZamora: Marker? = null
    private var mHuacana: Marker? = null
    private var msnJuan: Marker? = null
    private var mNocupetaro: Marker? = null
    private var mTancitaro: Marker? = null
    private var mHidalgo: Marker? = null
    private var mMaravatio: Marker? = null
    private var mHuetamo: Marker? = null
    private var mTuxpan: Marker? = null
    private var mMorelia: Marker? = null
    private var mZitacuaro: Marker? = null
   // private var mContepec: Marker? = null
    private var mTlalpujahua: Marker? = null
    private var mApatzingan: Marker? = null
    private var mAguililla: Marker? = null
    private var mBuenavista: Marker? = null
    private var mTepalcatepec: Marker? = null
   // private var mItalia: Marker? = null
    private var mCoalcoman: Marker? = null
    private var mEstado: Marker? = null
    private var mViolencia: Marker? = null
    private var mAltoImpacto: Marker? = null
    private var mPatzcuaro: Marker? = null
    private var mEstadoM: Marker? = null
    private var mZinapecuaro: Marker? = null
    /*private var locationManager : LocationManager? = null
    private var hasGSP=false*/



    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        mview=inflater.inflate(R.layout.fragment_fiscalias, container, false)
        if(!CheckPermissionFromDeviceCamera())
            requestPermissionCamera()

        locationManager = activity!!.getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager?;
     /*   locationManager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        hasGSP= locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)

        if (hasGSP ){

        }else{
            val builder = AlertDialog.Builder(this.context!!)
            builder.setTitle("Ubicación")
            builder.setMessage("Activa tu ubicación para obtener la información automáticamente")
            builder.setPositiveButton("Sí"){dialog, which ->
                //startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                val googleApiClient = GoogleApiClient.Builder(this.context!!)
                    .addApi(LocationServices.API).build()
                googleApiClient.connect()

                val locationRequest = LocationRequest.create()
                locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                /*locationRequest.interval = 10000
                locationRequest.fastestInterval = (10000 / 2).toLong()*/



                val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
                builder.setAlwaysShow(true)

                val result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
                result.setResultCallback(object : ResultCallback<LocationSettingsResult> {
                    override fun onResult(result: LocationSettingsResult) {
                        val status = result.status
                        when (status.statusCode) {
                            LocationSettingsStatusCodes.SUCCESS ->
                                print("Encendido")
                            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                                try {
                                    // Muestro el diálogo porque está apagado
                                    // in onActivityResult(). el número es por si quieres agregar un onActivityResult
                                    //Yo sólo quiero el GPS prendido y listo, so no hago caso a eso
                                    status.startResolutionForResult(activity, 101)

                                } catch (e: IntentSender.SendIntentException) {
                                }

                            }
                            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                                print("Error")

                        }
                    }

                })

            }
            builder.setNegativeButton("No"){dialog,which ->
               // Toast.makeText(context,"Rellena los campos manualmente", Toast.LENGTH_SHORT).show()
                dialog.dismiss()
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()

        }
*/
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.context!!)



        return mview
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mMapView= mview!!.findViewById(R.id.map)
        if (mMapView!=null){
            mMapView.onCreate(null)
            mMapView.onResume()
            mMapView.getMapAsync(this)
        }
    }

    //UBICACIÓN DEL USUARIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        MapsInitializer.initialize(context)
        mMap= p0!!
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL)
        mMap.moveCamera(CameraUpdateFactory.zoomTo(7.0f ))

        if(!CheckPermissionFromDevice())
            requestPermission()
        mMap.isMyLocationEnabled=true

        //si la ubicación permission is enabled

        fusedLocationClient!!.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                lastLocation = location
                currentLatLng = LatLng(location.latitude, location.longitude)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 9f))
                latitudMine=location.latitude
                longitudMine=location.longitude

            }else{
                mMap.moveCamera(CameraUpdateFactory.newLatLng(Patzcuaro))
            }

        }

//MARCADORESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

        mMap.run {
           /* val height = 100;
            val width = 70;
            val b = BitmapFactory.decodeResource(getResources(), R.drawable.apuntador);
            val smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
            val smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);*/


            mLazaro = mMap.addMarker(MarkerOptions().position(Lazaro).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
//.icon(BitmapDescriptorFactory.fromResource(R.drawable.lastname))
            mArteaga = mMap.addMarker(MarkerOptions().position(Arteaga).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mCoahuyana = mMap.addMarker(MarkerOptions().position(Coahuyana).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mZamora=mMap.addMarker(MarkerOptions().position(Zamora).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mReyes=mMap.addMarker(MarkerOptions().position(Reyes).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mZacapu=mMap.addMarker(MarkerOptions().position(Zacapu).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mPiedad=mMap.addMarker(MarkerOptions().position(Piedad).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mJiquilpan=mMap.addMarker(MarkerOptions().position(Jiquilpan).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mPuruandiro=mMap.addMarker(MarkerOptions().position(Puruandiro).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mSahuayo=mMap.addMarker(MarkerOptions().position(Sahuayo).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mTanhuato=mMap.addMarker(MarkerOptions().position(Tanhuato).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mUruapanSur=mMap.addMarker(MarkerOptions().position(UruaanSur).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mUruapanNor=mMap.addMarker(MarkerOptions().position(UruaanNor).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mParacho=mMap.addMarker(MarkerOptions().position(Paracho).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mTaretan=mMap.addMarker(MarkerOptions().position(Taretan).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mTacambaro=mMap.addMarker(MarkerOptions().position(Tacambaro).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mRosales=mMap.addMarker(MarkerOptions().position(Rosales).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mGZamora=mMap.addMarker(MarkerOptions().position(GZamora).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mHuacana=mMap.addMarker(MarkerOptions().position(Huacana).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            msnJuan=mMap.addMarker(MarkerOptions().position(snJuan).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mNocupetaro=mMap.addMarker(MarkerOptions().position(Nocupetaro).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mTancitaro=mMap.addMarker(MarkerOptions().position(Tancitaro).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mHidalgo=mMap.addMarker(MarkerOptions().position(Hdalgo).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mMaravatio=mMap.addMarker(MarkerOptions().position(Maravatio).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mHuetamo=mMap.addMarker(MarkerOptions().position(Huetamo).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mTuxpan=mMap.addMarker(MarkerOptions().position(Tuxpan).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mMorelia=mMap.addMarker(MarkerOptions().position(Morelia).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mZitacuaro=mMap.addMarker(MarkerOptions().position(Zitacuaro).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            //mContepec=mMap.addMarker(MarkerOptions().position(Contepec).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mTlalpujahua=mMap.addMarker(MarkerOptions().position(Tlalpujahua).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mApatzingan=mMap.addMarker(MarkerOptions().position(Apatzingan).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mAguililla=mMap.addMarker(MarkerOptions().position(Aguililla).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mBuenavista=mMap.addMarker(MarkerOptions().position(Buenavista).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mTepalcatepec=mMap.addMarker(MarkerOptions().position(Tepalcatepec).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
           // mItalia=mMap.addMarker(MarkerOptions().position(Italia).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mCoalcoman=mMap.addMarker(MarkerOptions().position(Coalcoman).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mPurepero=mMap.addMarker(MarkerOptions().position(Purepero).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mEstado=mMap.addMarker(MarkerOptions().position(Estado).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mViolencia=mMap.addMarker(MarkerOptions().position(Violencia).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mAltoImpacto=mMap.addMarker(MarkerOptions().position(AltoImpacto).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mPatzcuaro=mMap.addMarker(MarkerOptions().position(Patzcuaro).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mEstadoM=mMap.addMarker(MarkerOptions().position(EstadoMi).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))
            mZinapecuaro=mMap.addMarker(MarkerOptions().position(Zinapecuaro).icon(BitmapDescriptorFactory.fromResource(R.drawable.apuntador)))




            mMap.setOnMarkerClickListener(this@FiscaliasFragment);
        }


    }
    @SuppressLint("MissingPermission")
    override fun onMarkerClick(p0: Marker?): Boolean {


        // Check if a click count was set, then display the click count.
        if (p0 != null) {
            if(p0==mLazaro){
                val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                val mBuilder = android.app.AlertDialog.Builder(context)
                    .setView(mDialogView)
                val  mAlertDialog = mBuilder.show()
                //login button click of custom layout
                mDialogView.dialogLoginBtn.setOnClickListener {
                    mAlertDialog.dismiss()

                }
                mDialogView.ubicacionBotton.setOnClickListener {
                    /*
                        val uri = String.format(Locale.ENGLISH, "geo:%f,%f", 17.9604301,-102.19741)
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                        startActivity(intent)*/
                    val LATITUDE=17.9604301
                    val LONGITUDE=-102.19741

                    validar(LATITUDE, LONGITUDE)
                    /*val address = "http://maps.google.com/maps?q="+ LATITUDE  +"," + LONGITUDE +"("+ "FISCALIA REGIONAL LAZARO CARDENAS" + ")&iwloc=A&hl=es"
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(address))
                    startActivity(intent)*/

                }
                mDialogView.dialogCancelBtn.setOnClickListener {

                    val intent = Intent(Intent.ACTION_CALL)
                    intent.setData(Uri.parse("tel:" +"4433223600"))
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                    this.context!!.startActivity(intent)
                }
                mDialogView.imagenFiscalia.visibility=View.VISIBLE
                mDialogView.imagenFiscalia.setImageResource(R.drawable.lazaro_fiscalia)
                mDialogView.tituloF.setText(R.string.lazaro_titulo)
                mDialogView.ubicacionF.setText(R.string.lazaro_ubicacion)
                mDialogView.numeroF.setText(R.string.lazaro_numero)
            }
                if(p0==mArteaga){

                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this.context!!).setView(mDialogView)
                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=18.3568607
                        val LONGITUDE=-102.29263
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.arteaga_titulo)
                    mDialogView.ubicacionF.setText(R.string.arteaga_ubicacion)
                    mDialogView.numeroF.setText(R.string.arteaga_numero)


                }
                if(p0==mCoahuyana){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!).setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }

                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=18.6997773
                        val LONGITUDE=-103.6613
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223800"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this.context!!.startActivity(intent)
                    }
                    mDialogView.imagenFiscalia.visibility=View.VISIBLE
                    mDialogView.imagenFiscalia.setImageResource(R.drawable.cohauyana_fiscalia)
                    mDialogView.tituloF.setText(R.string.cohauyana_titulo)
                    mDialogView.ubicacionF.setText(R.string.cohauyana_ubicacion)
                    mDialogView.numeroF.setText(R.string.cohauyana_numero)


                }
                if(p0==mZamora){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)
                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=20.0519607
                        val LONGITUDE=-102.3263738
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this.context!!.startActivity(intent)
                    }
                    mDialogView.imagenFiscalia.visibility=View.VISIBLE
                    mDialogView.imagenFiscalia.setImageResource(R.drawable.zamora_fiscalia)
                    mDialogView.tituloF.setText(R.string.zamora_titulo)
                    mDialogView.ubicacionF.setText(R.string.zamora_ubicacion)
                    mDialogView.numeroF.setText(R.string.zamora_numero)


                }
                if(p0==mReyes){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)
                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }

                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE= 19.5869733
                        val LONGITUDE=-102.46954
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4333223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.reyes_titulo)
                    mDialogView.ubicacionF.setText(R.string.reyes_ubicacion)
                    mDialogView.numeroF.setText(R.string.reyes_numero)



                }
                if(p0==mZacapu){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.8151022
                        val LONGITUDE=-101.7869
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.zacapu_titulo)
                    mDialogView.ubicacionF.setText(R.string.zacapu_ubicacion)
                    mDialogView.numeroF.setText(R.string.zacapu_numero)


                }
                if (p0==mPurepero){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)
                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.8976994
                        val LONGITUDE=-101.99819
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }

                    mDialogView.tituloF.setText(R.string.purepero_titulo)
                    mDialogView.ubicacionF.setText(R.string.purepero_ubicacion)
                    mDialogView.numeroF.setText(R.string.purepero_numero)

                }
                if (p0==mPiedad){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this.context!!)
                        .setView(mDialogView)


                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=20.3354132
                        val LONGITUDE=-102.02035
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4333223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }

                    mDialogView.tituloF.setText(R.string.piedad_titulo)
                    mDialogView.ubicacionF.setText(R.string.piedad_ubicacion)
                    mDialogView.numeroF.setText(R.string.piedad_numero)

                }
                if (p0==mJiquilpan){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)


                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.990736
                        val LONGITUDE=-102.71725
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.imagenFiscalia.visibility=View.VISIBLE
                    mDialogView.imagenFiscalia.setImageResource(R.drawable.jiquilpan_fiscalia)
                    mDialogView.tituloF.setText(R.string.jiquilpan_titulo)
                    mDialogView.ubicacionF.setText(R.string.jiquilpan_ubicacion)
                    mDialogView.numeroF.setText(R.string.jiquilpan_numero)

                }
                if (p0==mPuruandiro){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=20.0826866
                        val LONGITUDE=-101.51554
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.puruandiro_titulo)
                    mDialogView.ubicacionF.setText(R.string.puruandiro_ubicacion)
                    mDialogView.numeroF.setText(R.string.puruandiro_numero)


                }
                if (p0==mSahuayo){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this.context!!)
                        .setView(mDialogView)
                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=20.048645
                        val LONGITUDE=-102.71433
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.zahuayo_titulo)
                    mDialogView.ubicacionF.setText(R.string.zahuayo_ubicacion)
                    mDialogView.numeroF.setText(R.string.zahuayo_numero)

                }
                if (p0==mTanhuato){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=20.2859444
                        val LONGITUDE=-102.32831
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.tanhuato_titulo)
                    mDialogView.ubicacionF.setText(R.string.tanhuato_ubicacion)
                    mDialogView.numeroF.setText(R.string.tanhuato_numero)

                }
                if (p0==mUruapanSur){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)


                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.4000853
                        val LONGITUDE=-102.0684774
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.imagenFiscalia.visibility=View.VISIBLE
                    mDialogView.imagenFiscalia.setImageResource(R.drawable.uruapan_regional)
                    mDialogView.tituloF.setText(R.string.uruapansur_titulo)
                    mDialogView.ubicacionF.setText(R.string.usuarpansur_ubicacion)
                    mDialogView.numeroF.setText(R.string.uruapansur_numero)

                }
               /* if (p0==mUruapanNor){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)


                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.4212025
                        val LONGITUDE=-102.0541
                        val intent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=$latitudMine,$longitudMine&daddr=$LATITUDE,$LONGITUDE"))
                        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity")
                        startActivity(intent)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    //mDialogView.imagenFiscalia.setImageResource(R.drawable.holaimagen)
                    mDialogView.tituloF.setText(R.string.uruapannorte_titulo)
                    mDialogView.ubicacionF.setText(R.string.usuarpannorte_ubicacion)
                    mDialogView.numeroF.setText(R.string.uruapannorte_numero)

                }*/
                if (p0==mParacho){

                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.6531015
                        val LONGITUDE=-102.04565
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.paracho_titulo)
                    mDialogView.ubicacionF.setText(R.string.paracho_ubicacion)
                    mDialogView.numeroF.setText(R.string.paracho_numero)


                }
                if (p0==mTaretan){

                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.3303692
                        val LONGITUDE=-101.92079
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4333223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.taretan_titulo)
                    mDialogView.ubicacionF.setText(R.string.taretan_ubicacion)
                    mDialogView.numeroF.setText(R.string.taretan_titulo)

                }
                if (p0==mTacambaro){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)


                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.2321718
                        val LONGITUDE=-101.46328
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.tacambaro_titulo)
                    mDialogView.ubicacionF.setText(R.string.tacambaro_ubicacion)
                    mDialogView.numeroF.setText(R.string.tacambaro_numero)

                }
                if (p0==mRosales){

                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.2060472
                        val LONGITUDE=-101.71075
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.rosales_titulo)
                    mDialogView.ubicacionF.setText(R.string.rosales_ubicacion)
                    mDialogView.numeroF.setText(R.string.rosales_numero)

                }
                if (p0==mGZamora){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.1577099
                        val LONGITUDE=-102.057
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.gzamora_titulo)
                    mDialogView.ubicacionF.setText(R.string.gzamora_ubicacion)
                    mDialogView.numeroF.setText(R.string.gzamora_numero)

                }
                if (p0==mHuacana){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=18.9617504
                        val LONGITUDE=-101.80851
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.huacana_titulo)
                    mDialogView.ubicacionF.setText(R.string.huacana_ubicacion)
                    mDialogView.numeroF.setText(R.string.huacana_numero)

                }
                if (p0==msnJuan){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)
                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.4162726
                        val LONGITUDE=-102.13189
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.sanjuan_titulo)
                    mDialogView.ubicacionF.setText(R.string.sanjuan_ubicacion)
                    mDialogView.numeroF.setText(R.string.sanjuan_numero)

                }
                if (p0==mNocupetaro){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.045706
                        val LONGITUDE=-101.163616
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                       Toast.makeText(context, "No aplica", Toast.LENGTH_SHORT).show()
                    }
                    mDialogView.tituloF.setText(R.string.nocupetaro_titulo)
                    mDialogView.ubicacionF.setText(R.string.nocupetaro_ubicacion)
                    mDialogView.numeroF.setText("No aplica")

                }
                if (p0==mTancitaro){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.336969
                        val LONGITUDE=-102.36339
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4333223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.tancitaro_titulo)
                    mDialogView.ubicacionF.setText(R.string.tancitaro_ubicacion)
                    mDialogView.numeroF.setText(R.string.tancitaro_numero)

                }
                if (p0==mHuetamo){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=18.628123
                        val LONGITUDE=-100.899939
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4355560423"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.imagenFiscalia.visibility=View.VISIBLE
                    mDialogView.imagenFiscalia.setImageResource(R.drawable.huetamo_fiscalia)
                    mDialogView.tituloF.setText(R.string.huetamo_titulo)
                    mDialogView.ubicacionF.setText(R.string.huetamo_ubicacion)
                    mDialogView.numeroF.setText(R.string.huetamo_numero)

                }
                if (p0==mMaravatio){

                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {

                        val LATITUDE= 19.695000
                        val LONGITUDE=-100.580056
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4474782133"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.maravatio_titulo)
                    mDialogView.ubicacionF.setText(R.string.maravatio_ubicacion)
                    mDialogView.numeroF.setText(R.string.maravatio_numero)

                }
                if (p0==mTuxpan){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)
                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.572013
                        val LONGITUDE=-100.467261
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.tuxpan_titulo)
                    mDialogView.ubicacionF.setText(R.string.tuxpan_ubicacion)
                    mDialogView.numeroF.setText(R.string.tuxpan_numero)

                }
                if (p0==mMorelia){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.682381
                        val LONGITUDE= -101.234842
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4333223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.morelia_titulo)
                    mDialogView.ubicacionF.setText(R.string.morelia_ubicacion)
                    mDialogView.numeroF.setText(R.string.morelia_numero)

                }
                if (p0==mZitacuaro){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.4295328
                        val LONGITUDE=-100.34283
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"7151680006"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }

                    mDialogView.imagenFiscalia.setImageResource(R.drawable.zitacuaro_fiscalia)
                    mDialogView.imagenFiscalia.visibility=View.VISIBLE
                    mDialogView.tituloF.setText(R.string.zitacuaro_titulo)
                    mDialogView.ubicacionF.setText(R.string.zitacuaro_ubicacion)
                    mDialogView.numeroF.setText(R.string.zitacuaro_numero)

                }
             /*   if (p0==mContepec){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.9500606
                        val LONGITUDE=-100.16794
                        val intent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=$latitudMine,$longitudMine&daddr=$LATITUDE,$LONGITUDE"))
                        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity")
                        startActivity(intent)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    //mDialogView.imagenFiscalia.setImageResource(R.drawable.holaimagen)
                    mDialogView.tituloF.setText(R.string.concatepec_titulo)
                    mDialogView.ubicacionF.setText(R.string.concatepec_ubicacion)
                    mDialogView.numeroF.setText(R.string.concatepec_numero)

                }*/
                if (p0==mTlalpujahua){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.803272
                        val LONGITUDE=-100.172142
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"7111580299"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.tlalpu_titulo)
                    mDialogView.ubicacionF.setText(R.string.tlalpu_ubicacion)
                    mDialogView.numeroF.setText(R.string.tlalpu_numero)

                }
                if (p0==mApatzingan){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.0870091
                        val LONGITUDE=-102.35098
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4535349950"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.imagenFiscalia.visibility=View.VISIBLE
                    mDialogView.imagenFiscalia.setImageResource(R.drawable.apatzingan)
                    mDialogView.tituloF.setText(R.string.apatzingan_titulo)
                    mDialogView.ubicacionF.setText(R.string.apatzingan_ubicacion)
                    mDialogView.numeroF.setText(R.string.apatzingan_numero)

                }
                if (p0==mAguililla){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=18.7359925
                        val LONGITUDE=-102.7928
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4265370423"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.aguililla_titulo)
                    mDialogView.ubicacionF.setText(R.string.aguililla_ubicacion)
                    mDialogView.numeroF.setText(R.string.aguililla_numero)

                }
                if (p0==mBuenavista){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)
                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.2106416
                        val LONGITUDE=-102.59079
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4265720732"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.vista_titulo)
                    mDialogView.ubicacionF.setText(R.string.vista_ubicacion)
                    mDialogView.numeroF.setText(R.string.vista_numero)

                }
                if (p0==mTepalcatepec){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.1892541
                        val LONGITUDE=-102.84554
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4245361791"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.tepalc_titulo)
                    mDialogView.ubicacionF.setText(R.string.tepalc_ubicacion)
                    mDialogView.numeroF.setText(R.string.tepalc_numero)

                }
                if (p0==mEstado){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)
                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.681413
                        val LONGITUDE=-101.234271
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.fiscal_titulo)
                    mDialogView.ubicacionF.setText(R.string.fiscal_ubicacion)
                    mDialogView.numeroF.setText(R.string.fiscal_numero)

                }
                if (p0==mViolencia){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.683514
                        val LONGITUDE=-101.234656
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4431477300"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.violencia_titulo)
                    mDialogView.ubicacionF.setText(R.string.violencia_ubicacion)
                    mDialogView.numeroF.setText(R.string.violencia_numero)

                }
                if (p0==mAltoImpacto){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.683802
                        val LONGITUDE=-101.234656
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4333223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.impacto_titulo)
                    mDialogView.ubicacionF.setText(R.string.impacto_ubicacion)
                    mDialogView.numeroF.setText(R.string.impacto_numero)

                }
                if (p0==mPatzcuaro){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.5107859
                        val LONGITUDE=-101.6169495
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4433223600"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.patz_titulo)
                    mDialogView.ubicacionF.setText(R.string.patz_ubicacion)
                    mDialogView.numeroF.setText(R.string.patz_numero)

                }
                if (p0==mZinapecuaro){
                    val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                    val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                        .setView(mDialogView)

                    val  mAlertDialog = mBuilder.show()
                    mDialogView.dialogLoginBtn.setOnClickListener {
                        mAlertDialog.dismiss()

                    }
                    mDialogView.ubicacionBotton.setOnClickListener {
                        val LATITUDE=19.8536089
                        val LONGITUDE=-100.8272261
                        validar(LATITUDE, LONGITUDE)
                    }
                    mDialogView.dialogCancelBtn.setOnClickListener {

                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" +"4513550049"))
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                        this!!.context!!.startActivity(intent)
                    }
                    mDialogView.tituloF.setText(R.string.zina_titulo)
                    mDialogView.ubicacionF.setText(R.string.zina_ubicacion)
                    mDialogView.numeroF.setText(R.string.zina_numero)
                }
                if (p0==mEstadoM){
                val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                    .setView(mDialogView)

                val  mAlertDialog = mBuilder.show()
                mDialogView.dialogLoginBtn.setOnClickListener {
                    mAlertDialog.dismiss()

                }
                mDialogView.ubicacionBotton.setOnClickListener {
                    val LATITUDE=19.674493
                    val LONGITUDE=-101.220608
                    validar(LATITUDE, LONGITUDE)
                }
                mDialogView.dialogCancelBtn.setOnClickListener {

                    val intent = Intent(Intent.ACTION_CALL)
                    intent.setData(Uri.parse("tel:" +"4333223600"))
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                    this!!.context!!.startActivity(intent)
                }
                mDialogView.tituloF.setText(R.string.huerta_titulo)
                mDialogView.ubicacionF.setText(R.string.huerta_ubicacion)
                mDialogView.numeroF.setText(R.string.huerta_numero)

            }
                if (p0==mHidalgo){
                val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                    .setView(mDialogView)

                val  mAlertDialog = mBuilder.show()
                mDialogView.dialogLoginBtn.setOnClickListener {
                    mAlertDialog.dismiss()

                }
                mDialogView.ubicacionBotton.setOnClickListener {
                    val LATITUDE= 19.045706
                    val LONGITUDE=-101.163616
                    validar(LATITUDE, LONGITUDE)
                }
                mDialogView.dialogCancelBtn.setOnClickListener {

                    val intent = Intent(Intent.ACTION_CALL)
                    intent.setData(Uri.parse("tel:" +"4433223600"))
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                    this!!.context!!.startActivity(intent)
                }
                mDialogView.tituloF.setText(R.string.hidalgo_titulo)
                mDialogView.ubicacionF.setText(R.string.hidalgo_ubicacion)
                mDialogView.numeroF.setText(R.string.hidalgo_numero)

            }
                if (p0==mCoalcoman){
                val mDialogView = LayoutInflater.from(context).inflate(R.layout.boton_llamada, null)
                val mBuilder = android.app.AlertDialog.Builder(this!!.context!!)
                    .setView(mDialogView)

                val  mAlertDialog = mBuilder.show()
                mDialogView.dialogLoginBtn.setOnClickListener {
                    mAlertDialog.dismiss()

                }
                mDialogView.ubicacionBotton.setOnClickListener {
                    val LATITUDE=18.7785571
                    val LONGITUDE=-103.16187
                    validar(LATITUDE, LONGITUDE)
                }
                mDialogView.dialogCancelBtn.setOnClickListener {

                    val intent = Intent(Intent.ACTION_CALL)
                    intent.setData(Uri.parse("tel:" +"4245330391"))
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                    this!!.context!!.startActivity(intent)
                }
                    mDialogView.tituloF.setText(R.string.coalco1)
                    mDialogView.ubicacionF.setText(R.string.coalcoman_ubicacion)
                    mDialogView.numeroF.setText(R.string.coalcoman_numero)

            }

        }
        return false
        }

    @SuppressLint("MissingPermission")
    private fun validar(latitude: Double, longitude: Double) {
        locationManager= activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        hasGSP= locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        hasNetwork=locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)


        if (hasGSP || hasNetwork){
            if (hasGSP){
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=$latitudMine,$longitudMine&daddr=$latitude,$longitude")
                )
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity")
                startActivity(intent)
            }
            if (hasNetwork){

                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=$latitudMine,$longitudMine&daddr=$latitude,$longitude")
                )
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity")
                startActivity(intent)
            }


        }else{

            val mDialogView = LayoutInflater.from(context).inflate(R.layout.encender_ubicacion, null)
            val mBuilder = android.app.AlertDialog.Builder(context).setView(mDialogView)
            val  mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            mDialogView.noPrenderGPS.setOnClickListener {
                Toast.makeText(context,"No podrá acceder a la ubicación de a Fiscalía",Toast.LENGTH_SHORT).show()
                mAlertDialog.dismiss()
            }
            mDialogView.siPrenderGPS.setOnClickListener {
                mAlertDialog.dismiss()
                //startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                val googleApiClient = GoogleApiClient.Builder(context!!)
                    .addApi(LocationServices.API).build()
                googleApiClient.connect()

                val locationRequest = LocationRequest.create()
                locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

                val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
                builder.setAlwaysShow(true)

                val result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
                result.setResultCallback(object : ResultCallback<LocationSettingsResult> {
                    override fun onResult(result: LocationSettingsResult) {
                        val status = result.status
                        when (status.statusCode) {
                            LocationSettingsStatusCodes.SUCCESS ->
                                print("Encendido")
                            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                                try {
                                    // Muestro el diálogo porque está apagado
                                    // in onActivityResult(). el número es por si quieres agregar un onActivityResult
                                    //Yo sólo quiero el GPS prendido y listo, so no hago caso a eso
                                    status.startResolutionForResult(activity, 101)
                                } catch (e: IntentSender.SendIntentException) {
                                }

                            }
                            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                                print("Error")

                        }
                    }
                })

            }

        }

    }


    private fun requestPermissionCamera() {
        ActivityCompat.requestPermissions(
            this!!.context!! as AppCompatActivity,
            arrayOf(
                Manifest.permission.CALL_PHONE,
                Manifest.permission.CALL_PRIVILEGED

            ),call_code_request)
    }

    private fun CheckPermissionFromDeviceCamera(): Boolean {
        val camera_permission= ContextCompat.checkSelfPermission(this!!.context!!, Manifest.permission.CALL_PHONE)
        val privileged= ContextCompat.checkSelfPermission(this!!.context!!, Manifest.permission.CALL_PRIVILEGED)

        return camera_permission== PackageManager.PERMISSION_GRANTED && privileged== PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(context as AppCompatActivity,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION

            ),RECORD_REQUEST_CODE)
    }

    private fun CheckPermissionFromDevice(): Boolean {
        val fine= ContextCompat.checkSelfPermission(context as AppCompatActivity, Manifest.permission.ACCESS_FINE_LOCATION)

        return  fine== PackageManager.PERMISSION_GRANTED
    }


}

