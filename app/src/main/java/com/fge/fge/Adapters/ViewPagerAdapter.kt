package com.fge.fge.Adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.fge.fge.DetallesActivity
import com.squareup.picasso.Picasso

class ViewPagerAdapter(private val context: Context, private val lalala: ArrayList<String>) : PagerAdapter() {
    override fun getCount(): Int {
        return lalala.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = ImageView(context)
        Picasso.get()
            .load(lalala[position])
            .fit()
            .centerCrop()
            .into(imageView)
        container.addView(imageView)

        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

}
