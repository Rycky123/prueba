package com.fge.fge.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fge.fge.R
import java.util.ArrayList


internal class RecyclerViewAdapter(val values: ArrayList<String>?, val context: Context) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_layout_denuncia, parent, false))
    }


    override fun onBindViewHolder(holder: RecyclerViewAdapter.ViewHolder, position: Int) {
        holder.name.text = values!![position]

        holder.name.setOnClickListener({
            Toast.makeText(context, "Hola: "+holder.name.text, Toast.LENGTH_SHORT).show()
        })
    }

    override fun getItemCount(): Int {
        return values!!.size
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView

        init {
            name = itemView.findViewById<View>(R.id.userIdentificador) as TextView
        }
    }
}



