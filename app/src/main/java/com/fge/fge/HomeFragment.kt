package com.fge.fge


import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.fge.fge.Models.GlobalClass
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.getkeepsafe.taptargetview.TapTargetView
import kotlinx.android.synthetic.main.activity_inicio.*
import kotlinx.android.synthetic.main.fragment_home.*
import android.R.menu
import android.view.MenuInflater


// Activity para inflar la pagina principal.
class HomeFragment : Fragment() {
    lateinit var predenuncia:Button
    lateinit var llamar:LinearLayout
    var view2: View? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
         view2=inflater.inflate(R.layout.fragment_home, container, false)
        predenuncia=view2!!.findViewById(R.id.predenuncia)
        llamar=view2!!.findViewById(R.id.llamarHomeFragment)


        showFabPrompt()

        predenuncia.setOnClickListener({
            val intento1 = Intent(context, FormularioPredenunciaActivity::class.java)
            startActivity(intento1)
            activity!!.overridePendingTransition(R.anim.fui_slide_in_right, R.anim.fui_slide_out_left)
        })

        llamar.setOnClickListener({
            val intent = Intent(Intent.ACTION_CALL)
            intent.setData(Uri.parse("tel:" +"4433223600"))
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION)
            startActivity(intent)
            Toast.makeText(context, "Llamando...", Toast.LENGTH_SHORT).show()
        })


        return view2
    }

    private fun showFabPrompt() {
        val prefManager = PreferenceManager.getDefaultSharedPreferences(context)

      /* val sequence = TapTargetSequence(activity)
            .targets(
                // This tap target will target the back button, we just need to pass its containing toolbar
                TapTarget.forView(view2!!.findViewById<View>(R.id.llamarHomeFragment), "Llamada 24 Horas", "Comuníquese con a cualquier hora del día.")
                    .cancelable(false)
                    .drawShadow(true)
                    .targetCircleColor(R.color.colorPrimaryDark)
                    .tintTarget(false)
                    .transparentTarget(false)
                    .textColor(android.R.color.white)
                    .dimColor(R.color.Dorado)
                    .outerCircleColor(R.color.colorAccent)
                    .id(1))/*,
                TapTarget.forToolbarMenuItem(toolbar, R.id.search, "Icono de búsqueda",
                    "lalalalalalala")
                    .dimColor(android.R.color.black)
                    .outerCircleColor(R.color.colorAccent)
                    .targetCircleColor(android.R.color.black)
                    .transparentTarget(true)
                    .textColor(android.R.color.black)
                    .id(2))*/

       /* ,

        TapTarget.forToolbarOverflow(toolbar, "Auí te mostrara más opciones",
            "Pero ni sirven :(").id(3))
        .listener(object : TapTargetSequence.Listener {

            override fun onSequenceFinish() {
                Toast.makeText(context, "Mostró la guía", Toast.LENGTH_LONG).show()
            }

            override fun onSequenceStep(lastTarget: TapTarget, targetClicked: Boolean) {
                Log.d("TapTargetView", "Clicked on " + lastTarget.id())
            }

            override fun onSequenceCanceled(lastTarget: TapTarget) {
                val dialog = context?.let {
                    AlertDialog.Builder(it)
                        .setTitle("Uh oh")
                        .setMessage("Cancelaste")
                        .setPositiveButton("Oops", null).show()
                }
                TapTargetView.showFor(dialog,
                    TapTarget.forView(dialog!!.getButton(DialogInterface.BUTTON_POSITIVE), "Uh oh!",
                        "cancelaste " + lastTarget.id())
                        .cancelable(false)
                        .tintTarget(false), object : TapTargetView.Listener() {
                        override fun onTargetClick(view: TapTargetView) {
                            super.onTargetClick(view)
                            dialog.dismiss()
                        }
                    })
            }
        })
*/*/

        if (!prefManager.getBoolean("didShowPrompt", false)){
            TapTargetView.showFor(activity, TapTarget.forView(view2!!.findViewById<View>(R.id.predenuncia),
                "Botón Pre-Denuncia", "Inicia el proceso de Denuncia."
                )
                .cancelable(false)
                .drawShadow(true)
                .dimColor(R.color.Dorado)
                .outerCircleColor(R.color.colorAccent)
                .targetCircleColor(R.color.colorPrimaryDark)
                .tintTarget(false), object : TapTargetView.Listener() {
                override fun onTargetClick(view: TapTargetView) {
                    super.onTargetClick(view)
                    val prefEditor = prefManager.edit()
                    prefEditor.putBoolean("didShowPrompt", true)
                    prefEditor.apply()
                    showOtro()
                }

                override fun onOuterCircleClick(view: TapTargetView) {
                    super.onOuterCircleClick(view)
                    Toast.makeText(view.context, "tocaste fuera del circulo", Toast.LENGTH_SHORT).show()
                }

                override fun onTargetDismissed(view: TapTargetView, userInitiated: Boolean) {
                    Log.d("TapTargetViewSample", "You dismissed me :(")
                }
            })
        }else{
            //Toast.makeText(context, "Ya no muestro", Toast.LENGTH_LONG).show()
        }

    }

    private fun showOtro() {
        TapTargetView.showFor(activity, TapTarget.forView(view2!!.findViewById<View>(R.id.llamarHomeFragment),
            "Llamada 24 Horas", "Comuníquese con a cualquier hora del día.")
            .cancelable(false)
            .drawShadow(true)
            .dimColor(R.color.Dorado)
            .outerCircleColor(R.color.colorAccent)
            .targetCircleColor(R.color.colorPrimaryDark)
            .tintTarget(false), object : TapTargetView.Listener() {
            override fun onTargetClick(view: TapTargetView) {
                super.onTargetClick(view)
                /* val prefEditor = prefManager.edit()
                 prefEditor.putBoolean("didShowPrompt", true)
                 prefEditor.apply()*/
            }

            override fun onOuterCircleClick(view: TapTargetView) {
                super.onOuterCircleClick(view)
                Toast.makeText(view.context, "tocaste fuera del circulo", Toast.LENGTH_SHORT).show()
            }

            override fun onTargetDismissed(view: TapTargetView, userInitiated: Boolean) {
                Log.d("TapTargetViewSample", "You dismissed me :(")
            }
        })
    }


}
